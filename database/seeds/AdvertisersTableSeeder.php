<?php

use App\Models\Advertiser;
use Illuminate\Database\Seeder;

class AdvertisersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertiser = new Advertiser();
        $advertiser->name = 'lucky.online';
        $advertiser->status = Advertiser::STATUS_ACTIVE;
        $advertiser->api_id = 1;
        $advertiser->params = [
            'apiKey' => '-',
        ];
        $advertiser->save();

        $advertiser = new Advertiser();
        $advertiser->name = 'LeadVertex';
        $advertiser->status = Advertiser::STATUS_ACTIVE;
        $advertiser->api_id = 2;
        $advertiser->params = [
            'token' => '-',
            'webmasterID' => '-',
        ];
        $advertiser->save();
    }
}
