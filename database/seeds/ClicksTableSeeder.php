<?php

use App\Models\Click;
use App\Models\Stream;
use Illuminate\Database\Seeder;

class ClicksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 1000000; $i++) {
            $click = new Click();
            $click->id = Str::uuid();
            $click->stream_id = Stream::select(['id'])->inRandomOrder()->first()->id;
            $click->created_date = $this->randomDateInRange(new DateTime('2018-10-17 00:00:00'), new DateTime());
            $click->save();
        }
    }

    public function randomDateInRange(DateTime $start, DateTime $end) {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate->format('Y-m-d');
    }
}
