<?php

use App\Models\Click;
use App\Models\Lead;
use Illuminate\Database\Seeder;

class LeadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20000; $i++) {
            $lead = new Lead();
            $lead->click_id = Click::select(['id'])->inRandomOrder()->first()->id;
            $lead->status = rand(1,4);
            $lead->name = Str::random(10);
            $lead->phone = Str::random(10);
            $lead->ip = Str::random(10);
            $lead->cpa_id = Str::random(10);
            $lead->save();
        }
    }
}
