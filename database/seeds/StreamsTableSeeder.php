<?php

use App\Models\Stream;
use Illuminate\Database\Seeder;

class StreamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stream = new Stream();
        $stream->id = '9230bc3d-19d5-483b-8cf0-6b3b72c8b458';
        $stream->user_id = 1;
        $stream->offer_id = 1;
        $stream->save();

        $stream = new Stream();
        $stream->id = '9230bc3d-19d5-483b-8cf0-6b3b72c8b457';
        $stream->user_id = 1;
        $stream->offer_id = 2;
        $stream->save();

        $stream = new Stream();
        $stream->id = '9230bc3d-19d5-483b-8cf0-6b3b72c8b459';
        $stream->user_id = 1;
        $stream->offer_id = 3;
        $stream->save();

        $stream = new Stream();
        $stream->id = '9230bc3d-19d5-483b-8cf0-6b3b72c8b460';
        $stream->user_id = 1;
        $stream->offer_id = 4;
        $stream->save();

        $stream = new Stream();
        $stream->id = '9230bc3d-19d5-483b-8cf0-6b3b72c8b470';
        $stream->user_id = 1;
        $stream->offer_id = 5;
        $stream->save();
    }
}
