<?php

use App\Models\Offer;
use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offer = new Offer();
        $offer->advertiser_id = 1;
        $offer->name = 'Нейросистема 1';
        $offer->rate_cpa = 100;
        $offer->rate_webmaster = 50;
        $offer->description = 'Средство для похудения';
        $offer->params = [
            'campaignHash' => 'cbf0945d-c199-4bf7-97af-9ae17b84db29',
        ];
        $offer->save();

        $offer = new Offer();
        $offer->advertiser_id = 1;
        $offer->name = 'Нейросистема 2';
        $offer->rate_cpa = 200;
        $offer->rate_webmaster = 100;
        $offer->description = 'Средство для похудения';
        $offer->params = [
            'campaignHash' => 'cbf0945d-c199-4bf7-97af-9ae17b84db29',
        ];
        $offer->save();

        $offer = new Offer();
        $offer->advertiser_id = 1;
        $offer->name = 'Нейросистема 3';
        $offer->rate_cpa = 300;
        $offer->rate_webmaster = 150;
        $offer->description = 'Средство для похудения';
        $offer->params = [
            'campaignHash' => 'cbf0945d-c199-4bf7-97af-9ae17b84db29',
        ];
        $offer->save();

        $offer = new Offer();
        $offer->advertiser_id = 1;
        $offer->name = 'Нейросистема 4';
        $offer->rate_cpa = 400;
        $offer->rate_webmaster = 200;
        $offer->description = 'Средство для похудения';
        $offer->params = [
            'campaignHash' => 'cbf0945d-c199-4bf7-97af-9ae17b84db29',
        ];
        $offer->save();

        $offer = new Offer();
        $offer->advertiser_id = 1;
        $offer->name = 'Нейросистема 5';
        $offer->rate_cpa = 500;
        $offer->rate_webmaster = 250;
        $offer->description = 'Средство для похудения';
        $offer->params = [
            'campaignHash' => 'cbf0945d-c199-4bf7-97af-9ae17b84db29',
        ];
        $offer->save();
    }
}
