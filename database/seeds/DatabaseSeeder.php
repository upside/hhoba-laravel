<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AdvertisersTableSeeder::class);
        $this->call(OffersTableSeeder::class);
        //$this->call(StreamsTableSeeder::class);
        //$this->call(ClicksTableSeeder::class);
        //$this->call(LeadsTableSeeder::class);
    }
}
