<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveOfferFieldsAndAddStreamField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn(['rate_cpa', 'rate_webmaster']);
        });

        Schema::table('streams', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('streams', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->decimal('rate_cpa')->nullable()->default(0);
            $table->decimal('rate_webmaster')->nullable()->default(0);
        });
    }
}
