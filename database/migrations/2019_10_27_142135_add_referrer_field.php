<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferrerField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->string('referer')->after('cpa_id')->nullable();
        });

        Schema::table('clicks', function (Blueprint $table) {
            $table->string('referer')->after('tracker_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('referer');
        });

        Schema::table('clicks', function (Blueprint $table) {
            $table->dropColumn('referer');
        });
    }
}
