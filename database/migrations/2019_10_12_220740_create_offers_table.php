<?php

use App\Models\Offer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('advertiser_id')->unsigned();
            $table->tinyInteger('status')->nullable()->default(Offer::STATUS_ACTIVE);
            $table->string('name');
            $table->decimal('rate_cpa')->nullable()->default(0);
            $table->decimal('rate_webmaster')->nullable()->default(0);
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->text('params')->nullable();
            $table->timestamps();

            $table->foreign('advertiser_id')->references('id')->on('advertisers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
