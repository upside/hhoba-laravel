<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clicks', function (Blueprint $table) {
            $table->bigInteger('geo_id')->unsigned()->nullable()->after('tracker_id');
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->bigInteger('geo_id')->unsigned()->nullable()->after('click_id');
            $table->bigInteger('cpa_geo_id')->unsigned()->nullable()->after('cpa_id');
        });

        DB::table('clicks')->update(['geo_id' => 174]);
        DB::table('leads')->update(['geo_id' => 174, 'cpa_geo_id' => 174]);

        Schema::table('clicks', function (Blueprint $table) {
            $table->bigInteger('geo_id')->nullable(false)->change();
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->bigInteger('geo_id')->nullable(false)->change();
            $table->bigInteger('cpa_geo_id')->unsigned()->nullable(false)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clicks', function (Blueprint $table) {
            $table->dropColumn('geo_id');
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('geo_id');
            $table->dropColumn('cpa_geo_id');
        });
    }
}
