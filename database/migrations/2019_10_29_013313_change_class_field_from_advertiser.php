<?php

use App\Models\Advertiser;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeClassFieldFromAdvertiser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Advertiser::whereClass(\App\AdvertisersApi\Lucky\LuckyApi::class)->update(['class'=>1]);

        Schema::table('advertisers', function (Blueprint $table) {
            $table->renameColumn('class', 'api_id');
        });

        Schema::table('advertisers', function (Blueprint $table) {
            $table->integer('api_id')->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advertisers', function (Blueprint $table) {
            $table->renameColumn('api_id', 'class');
        });

        Schema::table('advertisers', function (Blueprint $table) {
            $table->string('class')->change();
        });

        Advertiser::whereClass(1)->update(['class'=>\App\AdvertisersApi\Lucky\LuckyApi::class]);
    }
}
