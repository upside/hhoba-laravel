<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en')->nullable();
            $table->string('name_ru')->nullable();
            $table->string('abbreviation_ru')->nullable();
            $table->string('abbreviation_en')->nullable();
            $table->char('code', 3)->nullable();
            $table->decimal('rate')->nullable()->default(1);
            $table->timestamps();
        });

        $currencies = [
            [
                'name_en' => 'Ruble',
                'name_ru' => 'Рубль',
                'abbreviation_ru' => 'руб',
                'abbreviation_en' => 'rub',
                'code' => 'RUB',
                'rate' => '1',
            ],
            [
                'name_en' => 'Dollar',
                'name_ru' => 'Доллар',
                'abbreviation_ru' => 'дол',
                'abbreviation_en' => 'usd',
                'code' => 'USD',
                'rate' => '63.80',
            ],
        ];

        \Illuminate\Support\Facades\DB::table('currencies')->insert($currencies);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
