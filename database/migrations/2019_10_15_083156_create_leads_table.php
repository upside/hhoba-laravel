<?php

use App\Models\Lead;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('click_id', 36);
            $table->tinyInteger('status')->default(Lead::STATUS_NEW);
            $table->string('name');
            $table->string('phone');
            $table->ipAddress('ip')->nullable();
            $table->string('cpa_id')->nullable();
            $table->text('user_agent')->nullable();
            $table->timestamps();

            $table->foreign('click_id')->references('id')->on('clicks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
