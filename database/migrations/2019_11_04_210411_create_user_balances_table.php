<?php

use App\Models\Currency;
use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('currency_id')->unsigned();
            $table->decimal('amount')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        $users = User::all();
        $currency = Currency::where('code', '=', 'RUB')->firstOrFail();

        foreach ($users as $user){
            $userBalance = new UserBalance();
            $userBalance->user()->associate($user);
            $userBalance->currency()->associate($currency);
            $userBalance->amount = $user->balance;
            $userBalance->save();
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balances');

        Schema::table('users', function (Blueprint $table) {
            $table->decimal('balance')->nullable()->default(0);
        });
    }
}
