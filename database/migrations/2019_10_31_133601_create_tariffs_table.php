<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_id')->unsigned();
            $table->bigInteger('currency_id')->unsigned();
            $table->bigInteger('geo_id')->unsigned();
            $table->decimal('rate');
            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('geo_id')->references('id')->on('geo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}
