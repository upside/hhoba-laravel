const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

function inputCopy(el) {
    el.select();
    el.setSelectionRange(0, 99999);
    document.execCommand('copy');
    Toast.fire({
        type: 'success',
        title: el.value+' скопирован в буфер обмена'
    });
}

$(document).ready(function (e) {
    console.log('app');


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var hash = window.location.hash;
    //hash && $('ul.nav[href="' + hash + '"]').tab('show');

    $('a.nav-link[href="' + hash + '"]').click();

    $('a.nav-link').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });

    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });
});

