<?php

return [
    'telegram_error_bot_api_key' => env('TELEGRAM_ERROR_BOT_API_KEY'),
    'telegram_error_bot_username' => env('TELEGRAM_ERROR_BOT_USERNAME'),
];
