<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'lucky-online' => [
            'driver' => 'daily',
            'path' => storage_path('logs/cpa/lucky.online.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'LeadVertex' => [
            'driver' => 'daily',
            'path' => storage_path('logs/cpa/leadvertex.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'Everad' => [
            'driver' => 'daily',
            'path' => storage_path('logs/cpa/everad.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'telegram' => [
            'driver' => 'daily',
            'path' => storage_path('logs/telegram.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'syslog-telegram' => [
            'driver' => 'monolog',
            'handler' => Monolog\Handler\TelegramBotHandler::class,
            'with' => [
                'apiKey' => env('TELEGRAM_LOG_HANDLER_API_KEY'),
                'channel' => env('TELEGRAM_LOG_HANDLER_API_CHANNEL'),
            ],
            'permission' => 0666,
        ],

        'clickhouse' => [
            'driver' => 'daily',
            'path' => storage_path('logs/clickhouse.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'tracker' => [
            'driver' => 'daily',
            'path' => storage_path('logs/tracker/tracker.log'),
            'level' => 'debug',
            'days' => 14,
            'ignore_exceptions' => false,
            'permission' => 0666,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'permission' => 0666,
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
            'permission' => 0666,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
            'permission' => 0666,
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
            'permission' => 0666,
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
            'permission' => 0666,
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
            'permission' => 0666,
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
            'permission' => 0666,
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
            'permission' => 0666,
        ],
    ],

];
