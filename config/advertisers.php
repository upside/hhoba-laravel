<?php

return [
    'api' => [
        1 => \App\AdvertisersApi\Lucky\LuckyApi::class,
        2 => \App\AdvertisersApi\LeadVertex\LeadVertexApi::class,
        3 => \App\AdvertisersApi\Everad\EveradApi::class,
    ]
];
