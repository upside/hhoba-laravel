<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Geo
 *
 * @property int $id
 * @property string|null $name_en
 * @property string|null $name_ru
 * @property string|null $iso_2
 * @property string|null $iso_3
 * @property string|null $iso_num
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffCpa[] $cpaTariffs
 * @property-read int|null $cpa_tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tariff[] $tariffs
 * @property-read int|null $tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffUser[] $userTariffs
 * @property-read int|null $user_tariffs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereIso2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereIso3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereIsoNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Geo whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class Geo extends Model
{
    protected $table = 'geo';

    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function tariffs(): HasMany
    {
        return $this->hasMany(Tariff::class);
    }

    /**
     * @return HasMany
     */
    public function cpaTariffs(): HasMany
    {
        return $this->hasMany(TariffCpa::class);
    }

    /**
     * @return HasMany
     */
    public function userTariffs(): HasMany
    {
        return $this->hasMany(TariffUser::class);
    }

}
