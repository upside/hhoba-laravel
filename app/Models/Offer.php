<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Offer
 *
 * @property int $id
 * @property int $advertiser_id
 * @property int|null $status
 * @property string $name
 * @property float|null $rate_cpa
 * @property float|null $rate_webmaster
 * @property string|null $description
 * @property string|null $image
 * @property array|null $params
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Advertiser $advertiser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffCpa[] $cpaTariffs
 * @property-read int|null $cpa_tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stream[] $streams
 * @property-read int|null $streams_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tariff[] $tariffs
 * @property-read int|null $tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffUser[] $userTariffs
 * @property-read int|null $user_tariffs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereAdvertiserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereRateCpa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereRateWebmaster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Offer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Offer whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Offer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Offer withoutTrashed()
 */
class Offer extends Model
{
    use SoftDeletes;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_INACTIVE => 'Неактивный',
        self::STATUS_ACTIVE => 'Активный',
    ];

    protected $guarded = [];

    protected $casts = [
        'params' => 'array'
    ];

    /**
     * @return BelongsTo
     */
    public function advertiser(): BelongsTo
    {
        return $this->belongsTo(Advertiser::class);
    }

    /**
     * @return HasMany
     */
    public function streams(): HasMany
    {
        return $this->hasMany(Stream::class);
    }

    /**
     * @return HasMany
     */
    public function tariffs(): HasMany
    {
        return $this->hasMany(Tariff::class);
    }

    /**
     * @return HasMany
     */
    public function cpaTariffs(): HasMany
    {
        return $this->hasMany(TariffCpa::class);
    }

    /**
     * @return HasMany
     */
    public function userTariffs(): HasMany
    {
        return $this->hasMany(TariffUser::class);
    }

    /**
     * @param string|null $date_from
     * @param string|null $date_to
     * @return User|Builder|\Illuminate\Database\Query\Builder
     */
    public function statistics(string $date_from = null, string $date_to = null)
    {
        return $this->selectRaw('`c`.`created_date` as `date`')
            ->selectRaw('count(`c`.`id`) as `total_clicks`')
            ->selectRaw('count(`l`.`id`) as `total_leads`')
            ->selectRaw('count(if(`l`.`status` = ?, 1, null)) as `new_leads`', [Lead::STATUS_NEW])
            ->selectRaw('count(if(`l`.`status` = ?, 1, null)) as `approve_leads`', [Lead::STATUS_CONFIRM])
            ->selectRaw('count(if(`l`.`status` = ?, 1, null)) as `decline_leads`', [Lead::STATUS_DECLINE])
            ->selectRaw('count(if(`l`.`status` = ?, 1, null)) as `trash_leads`', [Lead::STATUS_TRASH])
            ->selectRaw('coalesce(round(count(if(`l`.`status` = ?, 1, null)) / count(`l`.`id`) * 100, 2), 0) as `cr`', [Lead::STATUS_CONFIRM])
            ->selectRaw('round(sum(if(`l`.`status` = ?, `offers`.`rate_cpa`, 0)) / count(`c`.`id`), 2) as `epc`', [Lead::STATUS_CONFIRM])
            ->selectRaw('coalesce(round(count(if(`l`.`status` = ?, 1, null)) / count(if(`l`.`status` IN (?, ?, ?), 1, null)) * 100, 2), 0) as `approve_percent`', [Lead::STATUS_CONFIRM, Lead::STATUS_CONFIRM, Lead::STATUS_DECLINE, Lead::STATUS_TRASH])
            ->selectRaw('sum(coalesce(`offers`.`rate_cpa`, 0)) as `money_total`')
            ->selectRaw('sum(if(`l`.`status` = ?, `offers`.`rate_cpa`, 0)) as `money_new`', [Lead::STATUS_NEW])
            ->selectRaw('sum(if(`l`.`status` = ?, `offers`.`rate_cpa`, 0)) as `money_approve`', [Lead::STATUS_CONFIRM])
            ->selectRaw('sum(if(`l`.`status` = ?, `offers`.`rate_cpa`, 0)) as `money_decline`', [Lead::STATUS_DECLINE])
            ->selectRaw('sum(if(`l`.`status` = ?, `offers`.`rate_cpa`, 0)) as `money_trash`', [Lead::STATUS_TRASH])

            ->leftJoin('streams as s', 's.offer_id', '=', 'offers.id')
            ->leftJoin('clicks as c', 'c.stream_id', '=', 's.id')
            ->leftJoin('leads as l', 'l.click_id', '=', 'c.id')

            ->where('offers.id', '=', $this->id)
            ->whereBetween('c.created_date', [$date_from, $date_to])
            ->groupBy('c.created_date');
    }
}
