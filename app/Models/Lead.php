<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Lead
 *
 * @property int $id
 * @property string $click_id
 * @property int $geo_id
 * @property int $status
 * @property string $name
 * @property string $phone
 * @property string $comment
 * @property string|null $ip
 * @property string|null $cpa_id
 * @property int $cpa_geo_id
 * @property string|null $referer
 * @property string|null $user_agent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Click $click
 * @property-read \App\Models\Geo $cpaGeo
 * @property-read \App\Models\Geo $geo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereClickId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCpaGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCpaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereUserAgent($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class Lead extends Model
{
    public const STATUS_NEW = 1;
    public const STATUS_CONFIRM = 2;
    public const STATUS_DECLINE = 3;
    public const STATUS_TRASH = 4;

    public const STATUSES = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_CONFIRM => 'Подтверждён',
        self::STATUS_DECLINE => 'Откланён',
        self::STATUS_TRASH => 'Трэш',
    ];

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function click(): BelongsTo
    {
        return $this->belongsTo(Click::class);
    }

    /**
     * @return BelongsTo
     */
    public function geo(): BelongsTo
    {
        return $this->belongsTo(Geo::class);
    }

    /**
     * @return BelongsTo
     */
    public function cpaGeo(): BelongsTo
    {
        return $this->belongsTo(Geo::class, 'cpa_geo_id', 'id');
    }
}
