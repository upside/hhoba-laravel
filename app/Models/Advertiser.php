<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Advertiser
 *
 * @property int $id
 * @property string $name
 * @property int|null $status
 * @property int $api_id
 * @property array|null $params
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Offer[] $offers
 * @property-read int|null $offers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advertiser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class Advertiser extends Model
{
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_INACTIVE => 'Неактивный',
        self::STATUS_ACTIVE => 'Активный',
    ];

    protected $guarded = [];

    protected $casts = [
        'params' => 'array',
    ];

    /**
     * @return HasMany
     */
    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class);
    }
}
