<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TariffUser
 *
 * @property int $id
 * @property int $offer_id
 * @property int $user_id
 * @property int $currency_id
 * @property int $geo_id
 * @property float $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\Geo $geo
 * @property-read \App\Models\Offer $offer
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffUser whereUserId($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class TariffUser extends Model implements TariffInterface
{
    protected $table = 'tariffs_user';
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return BelongsTo
     */
    public function geo(): BelongsTo
    {
        return $this->belongsTo(Geo::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getRate(): float
    {
        return (float)$this->rate;
    }
}
