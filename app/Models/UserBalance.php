<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserBalance
 *
 * @property int $id
 * @property int $user_id
 * @property int $currency_id
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereUserId($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class UserBalance extends Model
{
    protected $guarded = [];

    protected $with = ['currency'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }
}
