<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Statistics
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics query()
 * @mixin \Eloquent
 * @property string $click_date
 * @property string $click_id
 * @property string $stream_id
 * @property int $offer_id
 * @property string $offer_name
 * @property float $offer_ammount_rub
 * @property float $offer_ammount_usd
 * @property int $advertiser_id
 * @property string $advertiser_name
 * @property int $click_geo_id
 * @property string $click_geo_iso_2
 * @property string $click_geo_name_ru
 * @property int $webmaster_id
 * @property string $webmaster_email
 * @property int|null $lead_id
 * @property int|null $lead_status
 * @property int|null $lead_geo_id
 * @property string|null $lead_geo_iso_2
 * @property string|null $lead_geo_name_ru
 * @property float $lead_amount_rub
 * @property float $lead_amount_usd
 * @property-write mixed $raw
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereAdvertiserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereAdvertiserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereClickDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereClickGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereClickGeoIso2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereClickGeoNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereClickId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadAmountRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadAmountUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadGeoIso2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadGeoNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereLeadStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereOfferAmmountRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereOfferAmmountUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereOfferName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereWebmasterEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statistics whereWebmasterId($value)
 */
class Statistics extends Model
{
    protected $guarded = [];
}
