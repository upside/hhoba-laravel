<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\TariffCpa
 *
 * @property int $id
 * @property int $offer_id
 * @property int $currency_id
 * @property int $geo_id
 * @property float $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\Geo $geo
 * @property-read \App\Models\Offer $offer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TariffCpa whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class TariffCpa extends Model implements TariffInterface
{
    protected $table = 'tariffs_cpa';
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return BelongsTo
     */
    public function geo(): BelongsTo
    {
        return $this->belongsTo(Geo::class);
    }

    public function getRate(): float
    {
        return (float)$this->rate;
    }
}
