<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Click
 *
 * @property string $id
 * @property string $stream_id
 * @property string|null $tracker_id
 * @property int $geo_id
 * @property string|null $ip
 * @property string|null $referer
 * @property string $created_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Geo $geo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read int|null $leads_count
 * @property-read \App\Models\Stream $stream
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereCreatedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereGeoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereTrackerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Click whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class Click extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];

    /**
     * @return BelongsTo
     */
    public function stream(): BelongsTo
    {
        return $this->belongsTo(Stream::class);
    }

    /**
     * @return HasMany
     */
    public function leads(): HasMany
    {
        return $this->hasMany(Lead::class);
    }

    /**
     * @return BelongsTo
     */
    public function geo(): BelongsTo
    {
        return $this->belongsTo(Geo::class);
    }

}
