<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Stream
 *
 * @property string $id
 * @property string $name
 * @property int $user_id
 * @property int $offer_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Click[] $clicks
 * @property-read int|null $clicks_count
 * @property-read \App\Models\Offer $offer
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereUserId($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stream whereName($value)
 */
class Stream extends Model
{
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const STATUSES = [
        self::STATUS_INACTIVE => 'Неактивный',
        self::STATUS_ACTIVE => 'Активный',
    ];

    public $incrementing = false;
    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @return HasMany
     */
    public function clicks(): HasMany
    {
        return $this->hasMany(Click::class);
    }
}
