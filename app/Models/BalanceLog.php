<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\BalanceLog
 *
 * @property int $id
 * @property int $user_id
 * @property int $action
 * @property float $amount
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BalanceLog whereUserId($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class BalanceLog extends Model
{
    public const DEBIT = 1; //Списание
    public const CREDIT = 2; //Пополнение

    public const ACTIONS = [
        self::DEBIT => 'списание',
        self::CREDIT => 'пополнение',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
