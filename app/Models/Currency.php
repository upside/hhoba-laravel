<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Currency
 *
 * @property int $id
 * @property string|null $name_en
 * @property string|null $name_ru
 * @property string|null $abbreviation_ru
 * @property string|null $abbreviation_en
 * @property string|null $code
 * @property float|null $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffCpa[] $cpaTariffs
 * @property-read int|null $cpa_tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tariff[] $tariffs
 * @property-read int|null $tariffs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TariffUser[] $userTariffs
 * @property-read int|null $user_tariffs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereAbbreviationEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereAbbreviationRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Currency whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-write mixed $raw
 */
class Currency extends Model
{
    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function tariffs(): HasMany
    {
        return $this->hasMany(Tariff::class);
    }

    /**
     * @return HasMany
     */
    public function cpaTariffs(): HasMany
    {
        return $this->hasMany(TariffCpa::class);
    }

    /**
     * @return HasMany
     */
    public function userTariffs(): HasMany
    {
        return $this->hasMany(TariffUser::class);
    }
}
