<?php

namespace App\Models;

interface TariffInterface
{
    public function getRate(): float;
}
