<?php

namespace App\Console\Commands;

use App\Models\Click;
use App\Models\Lead;
use App\Models\Tariff;
use Illuminate\Console\Command;
use Tinderbox\Clickhouse\Client;
use Tinderbox\Clickhouse\Exceptions\ServerProviderException;
use Tinderbox\Clickhouse\Server;
use Tinderbox\Clickhouse\ServerProvider;
use Tinderbox\ClickhouseBuilder\Query\Builder;

/**
 * Class SyncClickHouse
 * @package App\Console\Commands
 */
class SyncClickHouse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:clickhouse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заполнение кликхауса данными';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws ServerProviderException
     */
    public function handle()
    {
        $server = new Server(
            config('database.connections.clickhouse.host'),
            config('database.connections.clickhouse.port'),
            config('database.connections.clickhouse.database'),
            config('database.connections.clickhouse.username'),
            config('database.connections.clickhouse.password')
        );
        $serverProvider = (new ServerProvider())->addServer($server);

        $client = new Client($serverProvider);
        $builder = new Builder($client);


        /** @var Click[] $clicks */
        Click::chunk(100, function ($clicks) use ($builder){
            $items = [];

            $i = 1;
            $t = 0;
            foreach ($clicks as $click){

                $click->load(['geo', 'stream.user', 'stream.offer.tariffs.geo', 'stream.offer.tariffs', 'stream.offer.advertiser']);

                $item['click_date'] = $click->created_date;
                $item['click_id'] = $click->id;
                $item['stream_id'] = $click->stream->id;
                $item['offer_id'] = (int)$click->stream->offer->id;
                $item['offer_name'] = $click->stream->offer->name;
                $item['advertiser_id'] = (int)$click->stream->offer->advertiser->id;
                $item['advertiser_name'] = $click->stream->offer->advertiser->name;
                $item['click_geo_id'] = (int)$click->geo->id;
                $item['click_geo_iso_2'] = $click->geo->iso_2;
                $item['click_geo_name_ru'] = $click->geo->name_ru;
                $item['webmaster_id'] = (int)$click->stream->user->id;
                $item['webmaster_email'] = $click->stream->user->email;
                $item['version'] = 1;

                /** @var Lead $lead */
                $lead = $click->leads()->first();

                if($lead){
                    /** @var Tariff $tariff */
                    $tariff = Tariff::where('offer_id', '=', $click->stream->offer->id)
                        ->where('geo_id', '=', $lead->geo->id)->first();

                    $item['lead_id'] = (int)$lead->id;
                    $item['lead_status'] = (int)$lead->status;
                    $item['lead_geo_id'] =(int) $lead->geo->id;
                    $item['lead_geo_iso_2'] = $lead->geo->iso_2;
                    $item['lead_geo_name_ru'] = $lead->geo->name_ru;
                    $item['lead_amount_rub'] = $tariff->currency_id == 1 ? (float)$tariff->rate : 0;
                    $item['lead_amount_usd'] = $tariff->currency_id == 2 ? (float)$tariff->rate : 0;
                }else{
                    $item['lead_id'] = 0;
                    $item['lead_status'] = 0;
                    $item['lead_geo_id'] = 0;
                    $item['lead_geo_iso_2'] = '';
                    $item['lead_geo_name_ru'] = '';
                    $item['lead_amount_rub'] = 0.0;
                    $item['lead_amount_usd'] = 0.0;
                }

                $items[] = $item;

                if($i == 100){

                    $builder->table('statistics')->insert($items);

                    echo 'Insert'.PHP_EOL;

                    $i = 1;
                }

                echo $t.PHP_EOL;
                $i++;
                $t++;
            }

        });
    }
}
