<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Tinderbox\Clickhouse\Client;
use Tinderbox\Clickhouse\Exceptions\ServerProviderException;
use Tinderbox\Clickhouse\Server;
use Tinderbox\Clickhouse\ServerProvider;
use Tinderbox\ClickhouseBuilder\Exceptions\GrammarException;
use Tinderbox\ClickhouseBuilder\Query\Builder;

/**
 * Class SyncRedisStatistics
 * @package App\Console\Commands
 */
class SyncRedisStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:redis-statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Переносим данные из редиса в кликхаус';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws GrammarException
     * @throws ServerProviderException
     */
    public function handle()
    {
        Log::channel('clickhouse')->info('Отправка данных в кликхаус');

        $json =  \RedisManager::get('clickhouse:data');

        if ($json) {
            $clickhouseData = json_decode($json, true);

            $server = new Server(
                config('database.connections.clickhouse.host'),
                config('database.connections.clickhouse.port'),
                config('database.connections.clickhouse.database'),
                config('database.connections.clickhouse.username'),
                config('database.connections.clickhouse.password')
            );
            $serverProvider = (new ServerProvider())->addServer($server);

            $client = new Client($serverProvider);
            $builder = new Builder($client);

            if($builder->table('statistics')->insert($clickhouseData)){
                \RedisManager::set('clickhouse:data', json_encode([]));
                Log::channel('clickhouse')->info('Данные ушли в кликхаус'.json_encode($clickhouseData));
            }else{
                Log::channel('clickhouse')->error('Что то не то'.json_encode($clickhouseData));
            }
        }

    }
}
