<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

/**
 * Class UpdateCurrencyRate
 * @package App\Console\Commands
 */
class UpdateCurrencyRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency-rate:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление курса валют';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        Log::channel('syslog-telegram')->info('Начинаем обновлять курсы валют');
        $xml = (new Client())->request('GET', 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . Carbon::now()->format('d/m/Y'))->getBody();

        $result = simplexml_load_string($xml);

        foreach ($result->Valute as $item) {
            try {
                $currency = Currency::where('code', '=', $item->CharCode)->firstOrFail();
                if ($currency) {
                    $currency->rate = (float)trim(str_replace(',', '.', $item->Value)) / (float)trim(str_replace(',', '.', $item->Nominal));
                    $currency->save();
                }
            } catch (Exception $exception) {
                report($exception);
            }
        }
        Log::channel('syslog-telegram')->info('Курсы валют обновленны');
    }
}
