<?php

namespace App\Observers;

use App\Events\AppEvent;
use App\Models\Lead;
use App\Models\Tariff;
use App\Services\ClickHouseService;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;

/**
 * Class LeadObserver
 * @package App\Observers
 */
class LeadObserver
{

    /**
     * @var ClickHouseService
     */
    private $clickHouseService;
    /**
     * @var UserService
     */
    private $userService;

    /**
     * LeadObserver constructor.
     * @param ClickHouseService $clickHouseService
     * @param UserService $userService
     */
    public function __construct(ClickHouseService $clickHouseService, UserService $userService)
    {
        $this->clickHouseService = $clickHouseService;
        $this->userService = $userService;
    }

    /**
     * Handle the lead "saving" event.
     *
     * @param Lead $lead
     * @return void
     * @todo провалидировать данные
     *
     */
    public function saving(Lead $lead)
    {

    }

    /**
     * Handle the lead "created" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        $tariff = Tariff::where('offer_id', '=', $lead->click->stream->offer->id)
            ->where('geo_id', '=', $lead->geo->id)->first();

        if ($tariff) {
            $this->clickHouseService->storeToRedis([
                'click_date' => $lead->click->created_date,
                'click_id' => $lead->click->id,
                'stream_id' => $lead->click->stream->id,
                'offer_id' => (int)$lead->click->stream->offer->id,
                'offer_name' => $lead->click->stream->offer->name,
                'advertiser_id' => (int)$lead->click->stream->offer->advertiser->id,
                'advertiser_name' => $lead->click->stream->offer->advertiser->name,
                'click_geo_id' => (int)$lead->click->geo->id,
                'click_geo_iso_2' => $lead->click->geo->iso_2,
                'click_geo_name_ru' => $lead->click->geo->name_ru,
                'webmaster_id' => (int)$lead->click->stream->user->id,
                'webmaster_email' => $lead->click->stream->user->email,
                'lead_id' => 0,
                'lead_status' => 0,
                'lead_geo_id' => 0,
                'lead_geo_iso_2' => '',
                'lead_geo_name_ru' => '',
                'lead_amount_rub' => 0.0,
                'lead_amount_usd' => 0.0,
                'version' => -1,
            ]);
            $this->clickHouseService->storeToRedis([
                'click_date' => $lead->click->created_date,
                'click_id' => $lead->click->id,
                'stream_id' => $lead->click->stream->id,
                'offer_id' => (int)$lead->click->stream->offer->id,
                'offer_name' => $lead->click->stream->offer->name,
                'advertiser_id' => (int)$lead->click->stream->offer->advertiser->id,
                'advertiser_name' => $lead->click->stream->offer->advertiser->name,
                'click_geo_id' => (int)$lead->click->geo->id,
                'click_geo_iso_2' => $lead->click->geo->iso_2,
                'click_geo_name_ru' => $lead->click->geo->name_ru,
                'webmaster_id' => (int)$lead->click->stream->user->id,
                'webmaster_email' => $lead->click->stream->user->email,
                'lead_id' => (int)$lead->id,
                'lead_status' => (int)$lead->status,
                'lead_geo_id' => (int)$lead->geo->id,
                'lead_geo_iso_2' => $lead->geo->iso_2,
                'lead_geo_name_ru' => $lead->geo->name_ru,
                'lead_amount_rub' => $tariff->currency_id == 1 ? (float)$tariff->rate : 0.0,
                'lead_amount_usd' => $tariff->currency_id == 2 ? (float)$tariff->rate : 0.0,
                'version' => 1,
            ]);
        } else {
            $message = 'Ненайден тарифф для оффера: ' . $lead->click->stream->offer->id . ' гео: ' . $lead->geo->iso_2;
            Log::error($message);
            Log::channel('syslog-telegram')->error($message);
        }

        broadcast(new AppEvent($lead->click->stream->user->id, AppEvent::LEAD_RECEIVE, $lead));

    }


    /**
     * Handle the lead "updating" event.
     *
     * @param Lead $lead
     *
     * @return void
     */
    public function updating(Lead $lead)
    {
        $tariff = Tariff::where('offer_id', '=', $lead->click->stream->offer->id)
            ->where('geo_id', '=', $lead->geo->id)->first();

        if ($tariff) {
            $this->clickHouseService->storeToRedis([
                'click_date' => $lead->click->created_date,
                'click_id' => $lead->click->id,
                'stream_id' => $lead->click->stream->id,
                'offer_id' => (int)$lead->click->stream->offer->id,
                'offer_name' => $lead->click->stream->offer->name,
                'advertiser_id' => (int)$lead->click->stream->offer->advertiser->id,
                'advertiser_name' => $lead->click->stream->offer->advertiser->name,
                'click_geo_id' => (int)$lead->click->geo->id,
                'click_geo_iso_2' => $lead->click->geo->iso_2,
                'click_geo_name_ru' => $lead->click->geo->name_ru,
                'webmaster_id' => (int)$lead->click->stream->user->id,
                'webmaster_email' => $lead->click->stream->user->email,
                'lead_id' => (int)$lead->id,
                'lead_status' => (int)$lead->getOriginal('status'),
                'lead_geo_id' => (int)$lead->geo->id,
                'lead_geo_iso_2' => $lead->geo->iso_2,
                'lead_geo_name_ru' => $lead->geo->name_ru,
                'lead_amount_rub' => $tariff->currency_id == 1 ? (float)$tariff->rate : 0.0,
                'lead_amount_usd' => $tariff->currency_id == 2 ? (float)$tariff->rate : 0.0,
                'version' => -1,
            ]);
        } else {
            $message = 'Ненайден тарифф для оффера: ' . $lead->click->stream->offer->id . ' гео: ' . $lead->geo->iso_2;
            Log::error($message);
            Log::channel('syslog-telegram')->error($message);
        }
    }

    /**
     * Handle the lead "updated" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function updated(Lead $lead)
    {
        /** @var Tariff $tariff */
        $tariff = Tariff::where('offer_id', '=', $lead->click->stream->offer->id)
            ->where('geo_id', '=', $lead->cpaGeo->id)
            ->first();

        if ($tariff) {
            $changes = $lead->getChanges();
            if (isset($changes['status'])) {
                if ($changes['status'] === Lead::STATUS_CONFIRM) {
                    $message = 'Пополнение баланса на сумму: ' . $tariff->getRate() . ' ' . $tariff->currency->abbreviation_ru . '., гео: ' . $lead->cpaGeo->iso_2 . ', лид: ' . $lead->id;
                    $this->userService->creditAccount($lead->click->stream->user, $tariff, $message);
                }
            }
            $this->clickHouseService->storeToRedis([
                'click_date' => $lead->click->created_date,
                'click_id' => $lead->click->id,
                'stream_id' => $lead->click->stream->id,
                'offer_id' => (int)$lead->click->stream->offer->id,
                'offer_name' => $lead->click->stream->offer->name,
                'advertiser_id' => (int)$lead->click->stream->offer->advertiser->id,
                'advertiser_name' => $lead->click->stream->offer->advertiser->name,
                'click_geo_id' => (int)$lead->click->geo->id,
                'click_geo_iso_2' => $lead->click->geo->iso_2,
                'click_geo_name_ru' => $lead->click->geo->name_ru,
                'webmaster_id' => (int)$lead->click->stream->user->id,
                'webmaster_email' => $lead->click->stream->user->email,
                'lead_id' => (int)$lead->id,
                'lead_status' => (int)$lead->status,
                'lead_geo_id' => (int)$lead->geo->id,
                'lead_geo_iso_2' => $lead->geo->iso_2,
                'lead_geo_name_ru' => $lead->geo->name_ru,
                'lead_amount_rub' => $tariff->currency_id == 1 ? (float)$tariff->rate : 0.0,
                'lead_amount_usd' => $tariff->currency_id == 2 ? (float)$tariff->rate : 0.0,
                'version' => 1,
            ]);
        } else {
            $message = 'Ненайден тарифф для оффера: ' . $lead->click->stream->offer->id . ' гео: ' . $lead->geo->iso_2;
            Log::error($message);
            Log::channel('syslog-telegram')->error($message);
        }
    }

    /**
     * Handle the lead "deleted" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function deleted(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "restored" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function restored(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "force deleted" event.
     *
     * @param Lead $lead
     * @return void
     */
    public function forceDeleted(Lead $lead)
    {
        //
    }
}
