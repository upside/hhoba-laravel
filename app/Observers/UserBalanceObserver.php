<?php

namespace App\Observers;

use App\Events\AppEvent;
use App\Models\UserBalance;

/**
 * Class UserBalanceObserver
 * @package App\Observers
 */
class UserBalanceObserver
{
    /**
     * Handle the lead "saving" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function saving(UserBalance $userBalance)
    {

    }

    /**
     * Handle the lead "created" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function created(UserBalance $userBalance)
    {
        broadcast(new AppEvent($userBalance->user_id, AppEvent::USER_BALANCE_UPDATE, $userBalance));
    }


    /**
     * Handle the lead "updating" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function updating(UserBalance $userBalance)
    {

    }

    /**
     * Handle the lead "updated" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function updated(UserBalance $userBalance)
    {
        broadcast(new AppEvent($userBalance->user_id, AppEvent::USER_BALANCE_UPDATE, $userBalance));
    }

    /**
     * Handle the lead "deleted" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function deleted(UserBalance $userBalance)
    {
        //
    }

    /**
     * Handle the lead "restored" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function restored(UserBalance $userBalance)
    {
        //
    }

    /**
     * Handle the lead "force deleted" event.
     *
     * @param UserBalance $userBalance
     * @return void
     */
    public function forceDeleted(UserBalance $userBalance)
    {
        //
    }
}
