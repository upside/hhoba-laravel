<?php


namespace App\MyTracker;

/**
 * Class MyTrackerParams
 * @package App\MyTracker
 */
class MyTrackerParams
{
    /**
     * @var string
     */
    private $subId;
    /**
     * @var int
     */
    private $payout;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var int
     */
    private $status;
    /**
     * @var string
     */
    private $from;

    /**
     * MyTrackerParams constructor.
     * @param string $subId
     * @param int $payout
     * @param string $currency
     * @param string $status
     * @param string $from
     */
    public function __construct(string $subId, int $payout, string $currency, string $status, string $from)
    {

        $this->subId = $subId;
        $this->payout = $payout;
        $this->currency = $currency;
        $this->status = $status;
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getSubId(): string
    {
        return $this->subId;
    }

    /**
     * @return int
     */
    public function getPayout(): int
    {
        return $this->payout;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

}
