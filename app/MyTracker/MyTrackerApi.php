<?php

namespace App\MyTracker;

use App\Models\Lead;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

/**
 * Class MyTrackerApi
 * @package App\MyTracker
 */
class MyTrackerApi
{
    public const STATUS_LEAD = 'lead';
    public const STATUS_SALE = 'sale';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_REBILL = 'rebill';

    public const STATUSES = [
        Lead::STATUS_NEW => self::STATUS_LEAD,
        Lead::STATUS_CONFIRM => self::STATUS_SALE,
        Lead::STATUS_DECLINE => self::STATUS_REJECTED,
        Lead::STATUS_TRASH => self::STATUS_REJECTED,
    ];
    /**
     * @var MyTrackerParams
     */
    private $params;

    /**
     * MyTrackerApi constructor.
     * @param MyTrackerParams $params
     */
    public function __construct(MyTrackerParams $params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send()
    {

        $client = new Client();

        $response = $client->request('POST', 'https://mytreker.ru/c3488a0/postback', [
            'query' => [
                'sub_id' => $this->params->getSubId(),
                'payout' => $this->params->getPayout(),
                'currency' => $this->params->getCurrency(),
                'status' => $this->params->getStatus(),
                'from' => $this->params->getFrom(),
            ],
        ]);

        Log::channel('lucky-online')->info('mytreker: ' . $response->getBody());

        return $response;

    }

}
