<?php

namespace App\Broadcasting;

use App\Models\User;

/**
 * Class UserChannel
 * @package App\Broadcasting
 */
class UserChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param \App\Models\User $user
     * @param int $userId
     * @return array|bool
     */
    public function join(User $user, int $userId)
    {
        return $user->id === $userId;
    }
}
