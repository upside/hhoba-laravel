<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GeoRequest
 * @package App\Http\Requests
 */
class GeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'nullable',
            'name_ru' => 'nullable',
            'iso_2' => 'nullable',
            'iso_3' => 'nullable',
            'iso_num' => 'nullable',
        ];
    }

}
