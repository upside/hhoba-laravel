<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CurrencyRequest
 * @package App\Http\Requests
 */
class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'nullable',
            'name_ru' => 'nullable',
            'abbreviation_ru' => 'nullable',
            'abbreviation_en' => 'nullable',
            'code' => 'nullable',
            'rate' => 'nullable',
        ];
    }

}
