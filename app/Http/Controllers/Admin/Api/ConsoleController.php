<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Models\Lead;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

/**
 * Class ConsoleController
 * @package App\Http\Controllers\Admin\Api
 */
class ConsoleController extends Controller
{

    /**
     * @return JsonResource
     */
    public function leadStat()
    {
        $y = (int)date('Y') - 1;
        $from = date($y . '-m-d');
        $to = date('Y-m-d');

        $data = Lead::whereBetween('created_at', [$from, $to])
            ->select([DB::raw('DATE(created_at) AS created_at'), 'status', DB::raw('COUNT(*) AS total')])
            ->groupBy([DB::raw('DATE(created_at)'), 'status'])
            ->orderBy(DB::raw('DATE(created_at)'))
            ->get();

        $stat = [];
        $datacollection = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Все',
                    'fill' => false,
                    'borderColor' => '#007bff',
                    'data' => [],
                ],
                [
                    'label' => 'Новый',
                    'fill' => false,
                    'borderColor' => '#ffc107',
                    'data' => [],
                ],
                [
                    'label' => 'Подтверждён',
                    'fill' => false,
                    'borderColor' => '#28a745',
                    'data' => [],
                ],
                [
                    'label' => 'Откланён',
                    'fill' => false,
                    'borderColor' => '#d81b60',
                    'data' => [],
                ],
                [
                    'label' => 'Трэш',
                    'fill' => false,
                    'borderColor' => '#dc3545',
                    'data' => [],
                ],
            ],
        ];
        foreach ($data as $item) {

            $date = $item->created_at->format('Y-m-d');

            if (!isset($stat[$date])) {
                $stat[$date] = [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                    'total' => 0,
                ];
            }

            if (!in_array($date, $datacollection['labels'])) {
                $datacollection['labels'][] = $date;
            }

            $stat[$date][$item->status] = $item->total;
            $stat[$date]['total'] += $item->total;

        }

        foreach ($datacollection['labels'] as $d) {
            $datacollection['datasets'][0]['data'][] = $stat[$d]['total'];
            $datacollection['datasets'][1]['data'][] = $stat[$d]['1'];
            $datacollection['datasets'][2]['data'][] = $stat[$d]['2'];
            $datacollection['datasets'][3]['data'][] = $stat[$d]['3'];
            $datacollection['datasets'][4]['data'][] = $stat[$d]['4'];
        }

        return new JsonResource($datacollection);
    }

}
