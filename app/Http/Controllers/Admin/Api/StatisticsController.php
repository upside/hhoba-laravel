<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Repositories\StatisticsRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class StatisticsController
 * @package App\Http\Controllers\Admin\Api
 */
class StatisticsController extends Controller
{

    /**
     * @param StatisticsRepository $statisticsRepository
     * @return JsonResponse
     */
    public function index(StatisticsRepository $statisticsRepository)
    {
        $user = Auth::user();

        $statistics = $statisticsRepository->getByUserId($user, Carbon::now()->subMonth(), Carbon::now());

        return response()->json($statistics);
    }
}
