<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\BalanceLogCollection;
use Illuminate\Support\Facades\Auth;

/**
 * Class BalanceLogController
 * @package App\Http\Controllers\Admin\Api
 */
class BalanceLogController extends Controller
{

    /**
     * @return BalanceLogCollection
     */
    public function index()
    {
        $balanceLogs = Auth::user()
            ->balanceLogs()
            ->paginate(100);

        return new BalanceLogCollection($balanceLogs);
    }
}
