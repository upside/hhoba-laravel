<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\ClickCollection;
use App\Models\Click;

/**
 * Class ClickController
 * @package App\Http\Controllers\Admin\Api
 */
class ClickController extends Controller
{

    /**
     * @return ClickCollection
     */
    public function index()
    {
        $clicks = Click::with(['geo'])
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return new ClickCollection($clicks);
    }

}
