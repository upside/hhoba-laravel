<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\GeoCollection;
use App\Http\Resources\GeoResource;
use App\Models\Geo;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class GeoController
 * @package App\Http\Controllers\Admin\Api
 */
class GeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return GeoCollection
     */
    public function index()
    {
        $geo = Geo::paginate(50);

        return new GeoCollection($geo);
    }

    /**
     * @return GeoCollection
     */
    public function all()
    {
        $geo = Geo::get();

        return new GeoCollection($geo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return GeoResource
     */
    public function store(Request $request)
    {
        $geo = new Geo();
        $geo->fill($request->all());
        $geo->save();

        return new GeoResource($geo);
    }

    /**
     * Display the specified resource.
     *
     * @param Geo $geo
     * @return GeoResource
     */
    public function show(Geo $geo)
    {
        return new GeoResource($geo);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Geo $geo
     * @return GeoResource
     */
    public function update(Request $request, Geo $geo)
    {
        $geo->fill($request->all());
        $geo->save();
        return new GeoResource($geo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Geo $geo
     * @return ResponseFactory|Response
     * @throws Exception
     */
    public function destroy(Geo $geo)
    {
        $geo->delete();

        return response('', 202);
    }
}
