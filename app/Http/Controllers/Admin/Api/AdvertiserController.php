<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\AdvertiserCollection;
use App\Models\Advertiser;

/**
 * Class AdvertiserController
 * @package App\Http\Controllers\Admin\Api
 */
class AdvertiserController  extends Controller
{
    /**
     * @return AdvertiserCollection
     */
    public function index()
    {
        $advertisers = Advertiser::paginate(50);

        return new AdvertiserCollection($advertisers);
    }

}
