<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\LeadCollection;
use App\Models\Lead;
use Illuminate\Support\Facades\Auth;

/**
 * Class LeadController
 * @package App\Http\Controllers\Admin\Api
 */
class LeadController extends Controller
{
    /**
     * @return LeadCollection
     */
    public function index()
    {
        if (Auth::user()->can('role', 'admin')) {
            $leads = Lead::with(['geo', 'cpaGeo'])
                ->orderBy('created_at', 'desc')
                ->paginate(50);
        } else {
            $leads = Auth::user()
                ->leads()
                ->with(['geo', 'cpaGeo'])
                ->orderBy('created_at', 'desc')
                ->paginate(50);
        }

        return new LeadCollection($leads);
    }

}
