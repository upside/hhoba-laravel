<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin\Api
 */
class UserController extends Controller
{
    /**
     * @return UserCollection
     */
    public function index()
    {
        return new UserCollection(User::paginate(50));
    }

    /**
     * @return UserCollection
     */
    public function all()
    {
        return new UserCollection(User::get());
    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        $user->load(['streams.offer']);

        return new UserResource($user);
    }

}
