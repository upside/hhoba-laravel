<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Requests\OfferRequest;
use App\Http\Resources\OfferCollection;
use App\Http\Resources\OfferResource;
use App\Models\Lead;
use App\Models\Offer;
use App\Services\OfferService;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Class OfferController
 * @package App\Http\Controllers\Admin\Api
 */
class OfferController extends Controller
{
    /**
     * @return OfferCollection
     */
    public function index()
    {
        $offers = Offer::with(['advertiser', 'tariffs.geo', 'tariffs.currency', 'cpaTariffs.geo', 'cpaTariffs.currency'])
            ->paginate(50);

        return new OfferCollection($offers);
    }

    /**
     * @param OfferService $offerService
     * @param OfferRequest $request
     * @return OfferResource
     */
    public function store(OfferService $offerService, OfferRequest $request)
    {
        return new OfferResource($offerService->createOrUpdate($request, new Offer()));
    }

    /**
     * @param Offer $offer
     * @return OfferResource
     */
    public function show(Offer $offer)
    {
        $offer->load([
            'advertiser',
            'tariffs.geo',
            'tariffs.currency',
            'cpaTariffs.geo',
            'cpaTariffs.currency',
            'streams.user',
        ]);

        return new OfferResource($offer);
    }

    /**
     * @param OfferService $offerService
     * @param OfferRequest $request
     * @param Offer $offer
     * @return OfferResource
     * @todo Добавить валидацию
     */
    public function update(OfferService $offerService, OfferRequest $request, Offer $offer)
    {
        return new OfferResource($offerService->createOrUpdate($request, $offer));
    }


    /**
     * @param Offer $offer
     * @return ResponseFactory|Response
     * @throws Exception
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();

        return response(null, 204);
    }


    //TODO: Стоит вынести в модель или сервис
    public function leadStat(Offer $offer)
    {
        $y = (int)date('Y') - 1;
        $from = date($y . '-m-d');
        $to = date('Y-m-d');

        $data = Lead::whereBetween('leads.created_at', [$from, $to])
            ->where('offers.id', '=', $offer->id)
            ->select([DB::raw('DATE(leads.created_at) AS created_at'), 'leads.status', DB::raw('COUNT(*) AS total')])
            ->join('clicks', 'clicks.id', '=', 'leads.click_id')
            ->join('streams', 'streams.id', '=', 'clicks.stream_id')
            ->join('offers', 'offers.id', '=', 'streams.offer_id')
            ->groupBy([DB::raw('DATE(leads.created_at)'), 'leads.status'])
            ->orderBy(DB::raw('DATE(leads.created_at)'))
            ->get();

        $stat = [];
        $datacollection = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Все',
                    'fill' => false,
                    'borderColor' => '#007bff',
                    'data' => [],
                ],
                [
                    'label' => 'Новый',
                    'fill' => false,
                    'borderColor' => '#ffc107',
                    'data' => [],
                ],
                [
                    'label' => 'Подтверждён',
                    'fill' => false,
                    'borderColor' => '#28a745',
                    'data' => [],
                ],
                [
                    'label' => 'Откланён',
                    'fill' => false,
                    'borderColor' => '#d81b60',
                    'data' => [],
                ],
                [
                    'label' => 'Трэш',
                    'fill' => false,
                    'borderColor' => '#dc3545',
                    'data' => [],
                ],
            ],
        ];
        foreach ($data as $item) {

            $date = $item->created_at->format('Y-m-d');

            if (!isset($stat[$date])) {
                $stat[$date] = [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                    'total' => 0,
                ];
            }

            if (!in_array($date, $datacollection['labels'])) {
                $datacollection['labels'][] = $date;
            }

            $stat[$date][$item->status] = $item->total;
            $stat[$date]['total'] += $item->total;
        }

        foreach ($datacollection['labels'] as $d) {
            $datacollection['datasets'][0]['data'][] = $stat[$d]['total'];
            $datacollection['datasets'][1]['data'][] = $stat[$d]['1'];
            $datacollection['datasets'][2]['data'][] = $stat[$d]['2'];
            $datacollection['datasets'][3]['data'][] = $stat[$d]['3'];
            $datacollection['datasets'][4]['data'][] = $stat[$d]['4'];
        }

        return new JsonResource($datacollection);
    }
}
