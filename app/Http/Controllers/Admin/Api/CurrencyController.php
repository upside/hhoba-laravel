<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\CurrencyCollection;
use App\Http\Resources\CurrencyResource;
use App\Models\Currency;

/**
 * Class CurrencyController
 * @package App\Http\Controllers\Admin\Api
 */
class CurrencyController extends Controller
{

    /**
     * @return CurrencyCollection
     */
    public function index()
    {
        $currencies = Currency::paginate(50);

        return new CurrencyCollection($currencies);
    }

    /**
     * @return CurrencyCollection
     */
    public function all()
    {
        $currencies = Currency::get();

        return new CurrencyCollection($currencies);
    }

    /**
     * @param Currency $currency
     * @return CurrencyResource
     */
    public function show(Currency $currency)
    {
        return new CurrencyResource($currency);
    }

}
