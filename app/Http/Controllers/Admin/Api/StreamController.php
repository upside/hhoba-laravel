<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controller;
use App\Http\Resources\StreamCollection;
use App\Http\Resources\StreamResource;
use App\Models\Offer;
use App\Models\Stream;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * Class StreamController
 * @package App\Http\Controllers\Admin\Api
 */
class StreamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return StreamCollection
     */
    public function index()
    {
        if (Auth::user()->can('role', 'admin')) {
            $streams = Stream::has('offer')
                ->with(['user', 'offer'])
                ->orderBy('created_at', 'desc')
                ->paginate(50);
        } else {
            $streams = Auth::user()
                ->streams()
                ->has('offer')
                ->with(['user', 'offer'])
                ->orderBy('created_at', 'desc')
                ->paginate(50);
        }

        return new StreamCollection($streams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return StreamResource
     * @throws Exception
     */
    public function store(Request $request)
    {
        $offer = Offer::findOrFail($request->get('offer_id'));

        $stream = new Stream();
        $stream->id = Str::uuid();
        $stream->name = $request->get('name');
        $stream->user()->associate(Auth::user());
        $stream->offer()->associate($offer);
        $stream->status = Stream::STATUS_ACTIVE;
        $stream->save();

        return new StreamResource($stream);
    }

    /**
     * Display the specified resource.
     *
     * @param Stream $stream
     * @return StreamResource
     */
    public function show(Stream $stream)
    {
        return new StreamResource($stream);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Stream $stream
     * @return StreamResource
     */
    public function update(Request $request, Stream $stream)
    {
        $stream->fill($request->all());
        $stream->save();
        return new StreamResource($stream);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Stream $stream
     * @return ResponseFactory|Response
     * @throws Exception
     */
    public function destroy(Stream $stream)
    {
        $stream->delete();

        return response('', 202);
    }

}
