<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controller;

/**
 * Class SpaController
 * @package App\Http\Controllers\Admin
 */
class SpaController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.dashboard.index');
    }

}
