<?php

namespace App\Http\Controllers\Api;

use App\Http\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class Click
 * @package App\Http\Controllers\Api
 */
class Cloak extends Controller
{

    /**
     * @param Request $request
     * @param string $apiKey
     * @param string $siteID
     * @return JsonResponse
     */
    public function index(Request $request, string $apiKey, string $siteID)
    {

        $landingData = json_decode($request->get('data'), true);

        $server = $landingData['_SERVER'];

        unset($server['REQUEST_TIME_FLOAT'], $server['REQUEST_TIME'], $server['HTTP_COOKIE']);

        return response()->json([
            'setCookie' => [
                'name' => 'test',
                'value' => 'bla',
                'expire' => time() + 3600,
            ],
            'data' => [
                'fingerprint' => md5(json_encode([
                    '_SERVER' => $server,
                    '_POST' => $landingData['_POST'],
                    '_GET' => $landingData['_GET'],
                ])),
                'landingData' => $landingData,
            ]
        ]);
    }

}
