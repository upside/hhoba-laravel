<?php

namespace App\Http\Controllers\Api;

use App\AdvertisersApi\AdvertiserApiFactory;
use App\Events\AppEvent;
use App\Http\Controller;
use App\Jobs\ProcessTrack;
use App\Models\Advertiser;
use App\Models\Geo;
use App\Models\Lead;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class PostBack
 * @package App\Http\Controllers\Api
 */
class PostBack extends Controller
{
    /**
     * @param Request $request
     * @param Advertiser $advertiser
     * @return JsonResponse
     * @throws \Exception
     */
    public function receive(Request $request, Advertiser $advertiser)
    {
        $api = AdvertiserApiFactory::getApi($advertiser->api_id);

        Log::channel($api->getName())->info(json_encode($request->input()));

        $lead = Lead::whereCpaId($request->input($api->getIdParam()))->first();

        if ($lead) {
            $g = $request->get($api->getCountryParam(), 'RU');

            if (empty($g)) {
                $g = 'RU';
            }

            $geo = Geo::where('iso_2', '=', $g)->first();
            $lead->cpa_geo_id = $geo->id;
            $lead->status = $api->getMappedStatus($request->input($api->getStatusParam()));
            $lead->save();
            Log::channel($api->getName())->warning('Lead cpa_id: ' . $request->input($api->getIdParam()) . ' status updated');
            broadcast(new AppEvent($lead->click->stream->user->id, 'LEAD_UPDATE_STATUS', $lead));
            if ($lead->click->tracker_id) {
                ProcessTrack::dispatch(6995, $lead->click->tracker_id, $lead->status, $lead->click->stream->offer->rate_cpa, $lead->referer);
            } else {
                Log::channel('syslog-telegram')->warning('Неуказан tracker_id для клика: ' . $lead->click->id . ' лида: ' . $lead->id);
            }

        } else {
            $message = $api->getName() . ' Lead cpa_id: ' . $request->input($api->getIdParam()) . ' not found';
            Log::channel($api->getName())->warning($message);
            Log::channel('syslog-telegram')->warning($message);
        }


        return response()->json('success');
    }

}
