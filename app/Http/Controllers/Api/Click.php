<?php

namespace App\Http\Controllers\Api;

use App\Http\Controller;
use App\Jobs\ProcessClick;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class Click
 * @package App\Http\Controllers\Api
 */
class Click extends Controller
{

    /**
     * @param Request $request
     * @param string $streamUUID
     * @return JsonResponse
     */
    public function deliver(Request $request, string $streamUUID)
    {
        $clickUUID = Str::uuid();
        ProcessClick::dispatch(
            $request->get('tracker_id', ''),
            $request->server('HTTP_REFERER', ''),
            $request->ip(),
            $clickUUID,
            $streamUUID
        );
        return response()->json($clickUUID);
    }

}
