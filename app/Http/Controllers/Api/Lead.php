<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CountryHelper;
use App\Http\Controller;
use App\Http\Requests\LeadRequest;
use App\Jobs\ProcessSendLead;
use App\Models\Geo;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Models\Lead as LeadModel;

/**
 * Class Lead
 * @package App\Http\Controllers\Api
 */
class Lead extends Controller
{
    /**
     * @param LeadRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function receive(LeadRequest $request)
    {
        $phone = preg_replace('/[^0-9]/', '', $request->get('phone', ''));

        try {
            $geoIso2Code = CountryHelper::getCountryCodeByPhone($phone);
            $geo = Geo::where('iso_2', '=', $geoIso2Code)->firstOrFail();
        } catch (Exception $exception) {
            $message = 'Неудалось определить гео для номера: ' . $phone;
            Log::warning($message);
            Log::channel('syslog-telegram')->warning($message);
            $geo = Geo::where('iso_2', '=', 'RU')->firstOrFail();
        }

        $lead = new LeadModel();
        $lead->fill($request->validated());
        $lead->comment = $request->get('comment');
        $lead->phone = $phone; //TODO: заменить на фильтры в валидаторе
        $lead->status = LeadModel::STATUS_NEW;
        $lead->ip = $request->ip();
        $lead->referer = $request->server('HTTP_REFERER', '');
        $lead->user_agent = $request->server('HTTP_USER_AGENT', '');
        $lead->geo()->associate($geo);
        $lead->cpaGeo()->associate($geo);
        $lead->save();

        ProcessSendLead::dispatch($lead);

        return response()->json($lead->id);
    }
}
