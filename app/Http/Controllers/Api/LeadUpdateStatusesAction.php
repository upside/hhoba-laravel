<?php

namespace App\Http\Actions\Api\Lead;

use App\AdvertisersApi\Lucky\LuckyApi;
use App\Http\Controller;
use App\Jobs\ProcessTrack;
use App\Models\Lead;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

class LeadUpdateStatusesAction extends Controller
{

    public const ROUTE_URI = '/lead/update-status/lucky';
    public const ROUTE_NAME = 'lead.update.status.lucky';

    public function __invoke()
    {
        $leadsToUpdate = Lead::getQuery()
            ->whereBetween('created_at', [Carbon::now()->subDay(), Carbon::now()])
            ->where('status', '=', Lead::STATUS_NEW)
            ->get();

        $cpaIds = $leadsToUpdate->pluck('cpa_id');

        $client = new Client();


        $response = $client->request('GET', 'https://lucky.online/api-webmaster/lead-status-batch.html', [
            'query' => [
                'api_key' => '5c321694cc770713347585777584',
                'click_id' => implode(',', $cpaIds->toArray()),
            ],
        ]);

        $data = json_decode($response->getBody(), true);

        foreach ($data['leads'] as $item) {
            $lead = Lead::where('cpa_id', '=', $item)->first();
            $lead->status = LuckyApi::STATUSES[$item['status']];
            $lead->update();

            echo 'Lead updated:' . $lead->id . PHP_EOL;

            if ($lead->click && $lead->click->tracker_id) {
                ProcessTrack::dispatch(6995, $lead->click->tracker_id, $lead->status, $lead->click->stream->offer->rate_cpa);
                echo 'Tracker send:' . $lead->click->tracker_id . PHP_EOL;
            }

        }

    }

}
