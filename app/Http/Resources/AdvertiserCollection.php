<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class AdvertiserCollection
 * @package App\Http\Resources
 */
class AdvertiserCollection extends ResourceCollection
{
}
