<?php

namespace App\Http\Resources;

use App\Models\Advertiser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AdvertiserResource
 * @package App\Http\Resources
 *
 * @mixin Advertiser
 */
class AdvertiserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($this);
    }
}
