<?php

namespace App\Http\Resources;

use App\Models\Click;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ClickResource
 * @package App\Http\Resources
 *
 * @mixin Click
 */
class ClickResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'stream_id' => $this->stream_id,
           'tracker_id' => $this->tracker_id,
           'geo_id' => $this->geo_id,
           'geo' => new GeoResource($this->geo),
           'ip' => $this->ip,
           'referer' => $this->referer,
           'created_date' => $this->created_date,
           'created_at' => $this->created_at,
           'updated_at' => $this->updated_at,
        ];
    }
}
