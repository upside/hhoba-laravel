<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class StatisticsCollection
 * @package App\Http\Resources
 */
class StatisticsCollection extends ResourceCollection
{
}
