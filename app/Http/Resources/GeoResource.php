<?php

namespace App\Http\Resources;

use App\Models\Geo;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GeoResource
 * @package App\Http\Resources
 *
 * @mixin Geo
 */
class GeoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($this);
    }
}
