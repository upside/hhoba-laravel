<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class TariffCpaCollection
 * @package App\Http\Resources
 */
class TariffCpaCollection extends ResourceCollection
{
}
