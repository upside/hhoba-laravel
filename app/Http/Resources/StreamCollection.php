<?php

namespace App\Http\Resources;

use App\Models\Stream;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class StreamCollection
 * @package App\Http\Resources
 * @property Stream[] $collection
 */
class StreamCollection extends ResourceCollection
{
}
