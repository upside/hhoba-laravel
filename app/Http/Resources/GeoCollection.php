<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class GeoCollection
 * @package App\Http\Resources
 */
class GeoCollection extends ResourceCollection
{
}
