<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class LeadCollection
 * @package App\Http\Resources
 */
class LeadCollection extends ResourceCollection
{
}
