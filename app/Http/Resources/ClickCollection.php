<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ClickCollection
 * @package App\Http\Resources
 */
class ClickCollection extends ResourceCollection
{
}
