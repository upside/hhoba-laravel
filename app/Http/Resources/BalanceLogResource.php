<?php

namespace App\Http\Resources;

use App\Models\BalanceLog;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BalanceLogResource
 * @package App\Http\Resources
 *
 * @mixin BalanceLog
 */
class BalanceLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($this);
    }
}
