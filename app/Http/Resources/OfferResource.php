<?php

namespace App\Http\Resources;

use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

/**
 * Class OfferResource
 * @package App\Http\Resources
 *
 * @mixin Offer
 */
class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'advertiser_id' => $this->advertiser_id,
            'advertiser' => new AdvertiserResource($this->advertiser),
            'status' => $this->status,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,
            'imageBase64' => $this->image ? Storage::disk('offers')->url($this->image) : null,
            'params' => $this->params,
            'tariffs' => new TariffCollection($this->tariffs),
            'cpa_tariffs' => new TariffCpaCollection($this->cpaTariffs),
            'user_tariffs' => new TariffCpaCollection($this->userTariffs),
            'streams' => new StreamCollection($this->streams),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
