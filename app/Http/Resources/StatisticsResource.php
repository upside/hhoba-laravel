<?php

namespace App\Http\Resources;

use App\Models\Statistics;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StatisticsResource
 * @package App\Http\Resources
 *
 * @mixin Statistics
 */
class StatisticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($this);
    }
}
