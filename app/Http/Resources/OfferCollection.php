<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class OfferCollection
 * @package App\Http\Resources
 */
class OfferCollection extends ResourceCollection
{
}
