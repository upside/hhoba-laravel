<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class CurrencyCollection
 * @package App\Http\Resources
 */
class CurrencyCollection extends ResourceCollection
{
}
