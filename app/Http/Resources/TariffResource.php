<?php

namespace App\Http\Resources;

use App\Models\Tariff;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TariffResource
 * @package App\Http\Resources
 *
 * @mixin Tariff
 */
class TariffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'offer_id' => $this->offer_id,
            'offer' => new OfferResource($this->offer),
            'currency_id' => $this->currency_id,
            'currency' => new CurrencyResource($this->currency),
            'geo_id' => $this->geo_id,
            'geo' => new GeoResource($this->geo),
            'rate' => $this->rate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
