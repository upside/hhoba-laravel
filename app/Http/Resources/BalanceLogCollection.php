<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BalanceLogCollection
 * @package App\Http\Resources
 */
class BalanceLogCollection extends ResourceCollection
{
}
