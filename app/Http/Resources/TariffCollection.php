<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class TariffCollection
 * @package App\Http\Resources
 */
class TariffCollection extends ResourceCollection
{
}
