<?php

namespace App\Http\Resources;

use App\Models\Stream;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StreamResource
 * @package App\Http\Resources
 *
 * @mixin Stream
 */
class StreamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => new UserResource($this->user),
            'offer_id' => $this->offer_id,
            'offer' => new OfferResource($this->offer),
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
