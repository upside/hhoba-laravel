<?php


namespace App\Helpers;


use Exception;

/**
 * Class CountryHelper
 * @package App\Helpers
 */
class CountryHelper
{

    public const COUNTRY = [
        'AB' => '/[78]?940\d{7}/',
        'HU' => '/36\d{8,9}/',
        'RU' => '/[78]?[3489]\d{9}/',
        'KZ' => '/[78]?7\d{9}/',
        'UA' => '/(38)?0\d{9}/',
        'AZ' => '/994[\d]{9,10}/',
        'AM' => '/374\d{8,9}/',
        'BY' => '/(375)|(80)\d{8,11}/',
        'GE' => '/995\d{8,10}/',
        'KG' => '/996\d{9,10}/',
        'MD' => '/3730?\d{8}/',
        'BG' => '/359\d{8,10}/',
        'LV' => '/371[2689]\d{7}/',
        'LT' => '/370\d{8}/',
        'EE' => '/372\d{8}/',
        'ES' => '/34\d{8,11}/',
        'IT' => '/39\d{6,11}/',
        'DE' => '/49\d{9,12}/',
        'AU' => '/61\d{9,10}/',
        'RO' => '/40\d{9,10}/',
        'GR' => '/30\d{10}/',
        'CY' => '/357\d{8}/',
        'PL' => '/48\d{9}/',
        'AT' => '/43\d{7,13}/',
        'PT' => '/351\d{9}/',
    ];


    /**
     * @param string $phone
     * @return string
     * @throws Exception
     */
    public static function getCountryCodeByPhone(string $phone): string
    {
        foreach (self::COUNTRY as $code => $pattern) {
            if (preg_match($pattern, $phone) === 1) {
                return $code;
            }
        }

        throw new Exception('Для номера: ' . $phone . ' ненайден код');

    }

}
