<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AppEvent
 * @package App\Events
 */
class AppEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public const USER_BALANCE_UPDATE = 'USER_BALANCE_UPDATE';
    public const LEAD_RECEIVE = 'LEAD_RECEIVE';
    public const LEAD_UPDATE_STATUS = 'LEAD_UPDATE_STATUS';

    public $type;
    public $data;
    private $user;


    /**
     * Create a new event instance.
     *
     * @param $user
     * @param $type
     * @param $data
     */
    public function __construct($user, $type, $data)
    {
        $this->user = $user;
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user.' . $this->user);
    }
}
