<?php

namespace App\Services;

/**
 * Class ClickHouseService
 * @package App\Services
 */
class ClickHouseService
{
    public const REDIS_KEY = 'clickhouse:data';

    /**
     * @param array $data
     */
    public function storeToRedis(array $data)
    {
        $json = \RedisManager::get(self::REDIS_KEY);

        $oldData = [];
        if ($json) {
            $oldData = json_decode($json, true);
        }
        $oldData[] = $data;

        \RedisManager::set(self::REDIS_KEY, json_encode($oldData));
    }

}
