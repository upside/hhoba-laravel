<?php

namespace App\Services;

use App\Models\BalanceLog;
use App\Models\TariffInterface;
use App\Models\User;
use App\Models\UserBalance;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{

    private const CREDIT = 'credit';
    private const DEBIT = 'debit';

    /**
     * @param User $user
     * @param TariffInterface $tariff
     * @param string|null $message
     * @return UserBalance
     */
    public function debitAccount(User $user, TariffInterface $tariff, ?string $message = null): UserBalance
    {
        return $this->debitOrCredit($user, $tariff, self::DEBIT, $message);
    }

    /**
     * @param User $user
     * @param TariffInterface $tariff
     * @param string|null $message
     * @return UserBalance
     */
    public function creditAccount(User $user, TariffInterface $tariff, ?string $message = null)
    {
        return $this->debitOrCredit($user, $tariff, self::CREDIT, $message);
    }


    /**
     * @param User $user
     * @param TariffInterface $tariff
     * @param string $operation
     * @param string|null $message
     * @return UserBalance
     */
    private function debitOrCredit(User $user, TariffInterface $tariff, string $operation, ?string $message = null)
    {
        $balance = UserBalance::firstOrNew([
            'user_id' => $user->id,
            'currency_id' => $tariff->currency_id,
        ]);

        if ($balance->exists) {
            switch ($operation) {
                case self::DEBIT:
                    $balance->decrement('amount', $tariff->getRate());
                    break;
                case self::CREDIT:
                    $balance->increment('amount', $tariff->getRate());
                    break;
            }
        } else {
            $balance->amount = $tariff->getRate();
        }

        $user->balances()->save($balance);

        $balanceLog = new BalanceLog();
        $balanceLog->user()->associate($user);
        $balanceLog->action = BalanceLog::CREDIT;
        $balanceLog->amount = $tariff->getRate();
        $balanceLog->description = $message;
        $balanceLog->save();

        return $balance;
    }

}
