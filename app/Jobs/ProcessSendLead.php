<?php

namespace App\Jobs;

use App\AdvertisersApi\AdvertiserApiFactory;
use App\Models\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProcessSendLead
 * @package App\Jobs
 */
class ProcessSendLead implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Lead
     */
    private $lead;

    /**
     * Create a new job instance.
     *
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
       $api = AdvertiserApiFactory::getApi($this->lead->click->stream->offer->advertiser->api_id);
       $api->send($this->lead);
    }
}
