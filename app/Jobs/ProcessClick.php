<?php

namespace App\Jobs;

use App\Models\Click;
use App\Models\Geo;
use App\Models\Stream;
use App\Services\ClickHouseService;
use Carbon\Carbon;
use Exception;
use GeoIp2\Database\Reader;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class ProcessClick
 * @package App\Jobs
 */
class ProcessClick implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var string
     */
    private $clickUUID;
    /**
     * @var string
     */
    private $streamUUID;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var string
     */
    private $trackerId;
    /**
     * @var string
     */
    private $httpReferer;
    /**
     * @var string
     */
    private $ip;

    /**
     * Create a new job instance.
     *
     * @param string|null $trackerId
     * @param string $httpReferer
     * @param string $ip
     * @param string $clickUUID
     * @param string $streamUUID
     */
    public function __construct(?string $trackerId, string $httpReferer, string $ip, string $clickUUID, string $streamUUID)
    {
        $this->clickUUID = $clickUUID;
        $this->streamUUID = $streamUUID;
        $this->trackerId = $trackerId ?? '';
        $this->httpReferer = $httpReferer;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @param ClickHouseService $clickHouseService
     * @return void
     */
    public function handle(ClickHouseService $clickHouseService)
    {

        try {
            $reader = new Reader(storage_path('GeoLite2-Country.mmdb'));
            $isoCode = $reader->country($this->ip)->country->isoCode;
            $geo = Geo::where('iso_2', '=', $isoCode)->firstOrFail();
        } catch (Exception $exception) {
            $message = 'Неудалось определить geo по ip: ' . $this->ip;
            Log::warning($message);
            Log::channel('syslog-telegram')->warning($message);
            $geo = Geo::where('iso_2', '=', 'RU')->firstOrFail();
        }

        $stream = Stream::findOrFail($this->streamUUID);

        $click = new Click();
        $click->id = $this->clickUUID;
        $click->stream()->associate($stream);
        $click->tracker_id = $this->trackerId;
        $click->geo()->associate($geo);
        $click->ip = $this->ip;
        $click->referer = $this->httpReferer;
        $click->created_date = Carbon::now()->format('Y-m-d');
        $click->save();

        $clickHouseService->storeToRedis([
            'click_date' => $click->created_date,
            'click_id' => $click->id,
            'stream_id' => $click->stream->id,
            'offer_id' => (int)$click->stream->offer->id,
            'offer_name' => $click->stream->offer->name,
            'advertiser_id' => (int)$click->stream->offer->advertiser->id,
            'advertiser_name' => $click->stream->offer->advertiser->name,
            'click_geo_id' => (int)$click->geo->id,
            'click_geo_iso_2' => $click->geo->iso_2,
            'click_geo_name_ru' => $click->geo->name_ru,
            'webmaster_id' => (int)$click->stream->user->id,
            'webmaster_email' => $click->stream->user->email,
            'lead_id' => 0,
            'lead_status' => 0,
            'lead_geo_id' => 0,
            'lead_geo_iso_2' => '',
            'lead_geo_name_ru' => '',
            'lead_amount_rub' => 0.0,
            'lead_amount_usd' => 0.0,
            'version' => 1,
        ]);
    }
}
