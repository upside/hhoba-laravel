<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request as TelegramRequest;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;

/**
 * Class ProcessSendTelegramError
 * @package App\Jobs
 */
class ProcessSendTelegramError implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Request
     */
    private $chatId;
    private $message;

    /**
     * Create a new job instance.
     *
     * @param $chatId
     * @param $message
     */
    public function __construct($chatId, $message)
    {
        $this->chatId = $chatId;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return bool|ServerResponse
     * @throws TelegramException
     */
    public function handle()
    {
        if (config('app.env') === 'local') {
            return true;
        }

        TelegramLog::initialize(Log::channel('telegram'));

        $telegram = new Telegram(
            config('hhoba.telegram_error_bot_api_key'),
            config('hhoba.telegram_error_bot_username')
        );

        TelegramRequest::initialize($telegram);

        return TelegramRequest::sendMessage([
            'chat_id' => $this->chatId,
            'text' => $this->message,
            'parse_mode' => 'HTML',
            'disable_web_page_preview' => true,
        ]);
    }
}
