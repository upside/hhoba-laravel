<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class ProcessTrack
 * @package App\Jobs
 */
class ProcessTrack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var int
     */
    private $userId;
    /**
     * @var string
     */
    private $cid;
    /**
     * @var float
     */
    private $payout;
    /**
     * @var string
     */
    private $status;
    /**
     * @var string|null
     */
    private $referer;

    /**
     * Create a new job instance.
     *
     * @param int $userId
     * @param string $cid
     * @param string $status
     * @param float|null $payout
     * @param string|null $referer
     */
    public function __construct(int $userId, string $cid, string $status, ?float $payout, ?string $referer)
    {
        $this->userId = $userId;
        $this->cid = $cid;
        $this->payout = $payout;
        $this->status = $status;
        $this->referer = $referer;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        $banner_id = '';
        $parts = parse_url($this->referer);
        if(isset($parts['query'])){
            parse_str($parts['query'], $query);
            $banner_id = $query['banner_id'] ?? '';
        }

        $client = new Client();

        $response = $client->request('POST', 'https://pstb.peerclicktrk.com/postback', [
            'query' => [
                'userid' => $this->userId,
                'cid' => $this->cid,
                'payout' => $this->payout,
                'status' => $this->status,
                'banner_id' => $banner_id,
            ],
        ]);

        Log::channel('tracker')->info($response->getBody());
    }
}
