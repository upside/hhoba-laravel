<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\CarbonInterface;
use Illuminate\Support\Collection;
use Tinderbox\Clickhouse\Client;
use Tinderbox\Clickhouse\Exceptions\ServerProviderException;
use Tinderbox\Clickhouse\Server;
use Tinderbox\Clickhouse\ServerProvider;
use Tinderbox\ClickhouseBuilder\Query\Builder;
use Tinderbox\ClickhouseBuilder\Query\Expression;

/**
 * Class StatisticsRepository
 * @package App\Repositories
 */
class StatisticsRepository
{
    /**
     * @var Builder
     */
    private $builder;

    /**
     * StatisticsService constructor.
     * @throws ServerProviderException
     */
    public function __construct()
    {
        $server = new Server(
            config('database.connections.clickhouse.host'),
            config('database.connections.clickhouse.port'),
            config('database.connections.clickhouse.database'),
            config('database.connections.clickhouse.username'),
            config('database.connections.clickhouse.password')
        );
        $serverProvider = (new ServerProvider())->addServer($server);
        $client = new Client($serverProvider);
        $this->builder = new Builder($client);
    }


    /**
     * @param CarbonInterface $dateFrom
     * @param CarbonInterface $dateTo
     * @return array
     */
    public function getAll(CarbonInterface $dateFrom, CarbonInterface $dateTo)
    {
        return $this->builder
            ->table('statistics')
            ->addSelect('click_date')
            ->addSelect('webmaster_id')
            ->addSelect('stream_id')
            ->addSelect(new Expression('if(count(if(lead_status = 2 AND version = 1, 1, null))- count(if(lead_status = 2 AND version = -1, 1, null)) > 0, round((count(if(lead_status = 2 AND version = 1, 1, null)) - count(if(lead_status = 2 AND version = -1, 1, null))) / count(if(lead_status <> 0, 1, null)) * 100, 2), 0) AS cr'))
            ->addSelect(new Expression('round(sum(if(lead_status = 2, lead_amount_rub * version, 0)) / count(click_id), 2) AS epc'))
            ->addSelect(new Expression('count(if(version = 1, 1, null)) - count(if(version = -1, 1, null))  AS clicks'))
            ->addSelect(new Expression('count(if(lead_status <> 0 AND version = 1, 1, null)) - count(if(lead_status <> 0 AND version = -1, 1, null)) AS leads'))
            ->addSelect(new Expression('count(if(lead_status = 1 AND version = 1, 1, null)) - count(if(lead_status = 1 AND version = -1, 1, null)) AS leads_new'))
            ->addSelect(new Expression('count(if(lead_status = 2 AND version = 1, 1, null)) - count(if(lead_status = 2 AND version = -1, 1, null)) AS leads_confirm'))
            ->addSelect(new Expression('count(if(lead_status = 3 AND version = 1, 1, null)) - count(if(lead_status = 3 AND version = -1, 1, null)) AS leads_decline'))
            ->addSelect(new Expression('count(if(lead_status = 4 AND version = 1, 1, null)) - count(if(lead_status = 4 AND version = -1, 1, null)) AS leads_trash'))
            ->addSelect(new Expression('sum(lead_amount_rub * version) AS money_total'))
            ->addSelect(new Expression('sum(if(lead_status = 1, lead_amount_rub * version, 0)) AS money_new'))
            ->addSelect(new Expression('sum(if(lead_status = 2, lead_amount_rub * version, 0)) AS money_confirm'))
            ->addSelect(new Expression('sum(if(lead_status = 3, lead_amount_rub * version, 0)) AS money_decline'))
            ->addSelect(new Expression('sum(if(lead_status = 4, lead_amount_rub * version, 0)) AS money_trash'))
            ->whereBetween('click_date', [$dateFrom->format('Y-m-d'), $dateTo->format('Y-m-d')])
            ->groupBy(['click_date', 'webmaster_id', 'stream_id'])
            ->orderBy('click_date', 'desc')
            ->having('clicks', '>', 0)
            ->get()
            ->getRows();
    }


    /**
     * @param User $user
     * @param CarbonInterface $dateFrom
     * @param CarbonInterface $dateTo
     * @return Collection
     */
    public function getByUserId(User $user, CarbonInterface $dateFrom, CarbonInterface $dateTo)
    {
        $data = $this->builder
            ->table('statistics')
            ->addSelect(['click_date', 'stream_id', 'offer_name', 'click_geo_iso_2'])
            ->addSelect(new Expression('if(count(if(lead_status = 2 AND version = 1, 1, null))- count(if(lead_status = 2 AND version = -1, 1, null)) > 0, round((count(if(lead_status = 2 AND version = 1, 1, null)) - count(if(lead_status = 2 AND version = -1, 1, null))) / count(if(lead_status <> 0, 1, null)) * 100, 2), 0) AS cr'))
            ->addSelect(new Expression('round(sum(if(lead_status = 2, lead_amount_rub * version, 0)) / count(click_id), 2) AS epc'))
            ->addSelect(new Expression('count(if(version = 1, 1, null)) - count(if(version = -1, 1, null))  AS clicks'))
            ->addSelect(new Expression('count(if(lead_status <> 0 AND version = 1, 1, null)) - count(if(lead_status <> 0 AND version = -1, 1, null)) AS leads'))
            ->addSelect(new Expression('count(if(lead_status = 1 AND version = 1, 1, null)) - count(if(lead_status = 1 AND version = -1, 1, null)) AS leads_new'))
            ->addSelect(new Expression('count(if(lead_status = 2 AND version = 1, 1, null)) - count(if(lead_status = 2 AND version = -1, 1, null)) AS leads_confirm'))
            ->addSelect(new Expression('count(if(lead_status = 3 AND version = 1, 1, null)) - count(if(lead_status = 3 AND version = -1, 1, null)) AS leads_decline'))
            ->addSelect(new Expression('count(if(lead_status = 4 AND version = 1, 1, null)) - count(if(lead_status = 4 AND version = -1, 1, null)) AS leads_trash'))
            ->addSelect(new Expression('sum(lead_amount_rub * version) AS money_total'))
            ->addSelect(new Expression('sum(if(lead_status = 1, lead_amount_rub * version, 0)) AS money_new'))
            ->addSelect(new Expression('sum(if(lead_status = 2, lead_amount_rub * version, 0)) AS money_confirm'))
            ->addSelect(new Expression('sum(if(lead_status = 3, lead_amount_rub * version, 0)) AS money_decline'))
            ->addSelect(new Expression('sum(if(lead_status = 4, lead_amount_rub * version, 0)) AS money_trash'))
            ->where('webmaster_id', '=', $user->id)
            ->whereBetween('click_date', [$dateFrom->format('Y-m-d'), $dateTo->format('Y-m-d')])
            ->groupBy(['click_date', 'stream_id', 'offer_name', 'click_geo_iso_2'])
            ->orderBy('click_date', 'desc')
            ->having('clicks', '>', 0)
            ->get()
            ->getRows();

        $collection = collect($data);

        return $collection->map(function ($item, $key) {
            $item['clicks'] = (int)$item['clicks'];
            $item['leads'] = (int)$item['leads'];
            $item['leads_new'] = (int)$item['leads_new'];
            $item['leads_confirm'] = (int)$item['leads_confirm'];
            $item['leads_decline'] = (int)$item['leads_decline'];
            $item['leads_trash'] = (int)$item['leads_trash'];
            return $item;
        });
    }
}
