<?php

namespace App\AdvertisersApi\LeadVertex;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Models\Advertiser;
use App\Models\Lead;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

/**
 * Class LeadVertexApi
 * @package App\AdvertisersApi\LeadVertex
 */
class LeadVertexApi implements AdvertisersApiInterface
{
    public const STATUS_SPAM = 'spam';
    public const STATUS_RETURN = 'return';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_SHIPPED = 'shipped';
    public const STATUS_PAID = 'paid';

    public const STATUSES = [
        self::STATUS_SPAM => Lead::STATUS_TRASH,
        self::STATUS_RETURN => Lead::STATUS_DECLINE,
        self::STATUS_CANCELED => Lead::STATUS_DECLINE,
        self::STATUS_PROCESSING => Lead::STATUS_NEW,
        self::STATUS_ACCEPTED => Lead::STATUS_CONFIRM,
        self::STATUS_SHIPPED => Lead::STATUS_CONFIRM,
        self::STATUS_PAID => Lead::STATUS_CONFIRM,
    ];

    /**
     * @param Lead $lead
     * @throws GuzzleException
     */
    public function send(Lead $lead)
    {
        $client = new Client();
        $offer = $lead->click->stream->offer;
        $advertiser = $offer->advertiser;

        $formParams = [
            'externalID' => $lead->id,
            'externalWebmaster' => $lead->click->stream->user->id,
            'ip' => $lead->ip,
            'phone' => $lead->phone,
            'comment' => $lead->comment,
            'fio' => $lead->name,
            'goods' => [
                [
                    'goodID' => $offer->params['goodID'],
                    'quantity' => $offer->params['quantity'],
                    'price' => $offer->params['price']
                ]
            ]
        ];

        $query = [
            'token' => $advertiser->params['token'],
            'webmasterID' => $advertiser->params['webmasterID'],
        ];

        $response = $client->request('POST', $this->getApiUrl(), [
            'query' => $query,
            'form_params' => $formParams,
        ]);

        $data = json_decode($response->getBody(), true);

        if ($data['OK']) {
            $lead->cpa_id = $data['OK'];
            $lead->save();
            Log::channel($this->getName())->info($response->getBody());
        } else {
            Log::channel($this->getName())->error($response->getBody());
            Log::channel('syslog-telegram')->error('Неудалось отпраить лид: ' . $lead->id . ' Ответ от LeadVertex' . $response->getBody());
        }

    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'LeadVertex';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'http://manicbox.leadvertex.ru/api/webmaster/v2/addOrder.html';
    }

    /**
     * @return array
     */
    public function getOfferParams(): array
    {
        return [
            'goodID' => '',
            'quantity' => '',
            'price' => '',
        ];
    }

    /**
     * @return array
     */
    public function getAdvertiserParams(): array
    {
        return [
            'token' => '',
            'webmasterID' => '',
        ];
    }

    /**
     * @param Advertiser $advertiser
     * @return string
     */
    public function getPostBackUrl(Advertiser $advertiser): string
    {
        return route('post-back.receive', array_merge(['advertiser' => $advertiser], $this->getPostBackParams()));
    }

    /**
     * @return array
     */
    public function getPostBackParams(): array
    {
        return [
            'id' => '{{id}}',
            'datetime' => '{{datetime}}',
            'domain' => '{{domain}}',
            'price' => '{{price}}',
            'payment_status' => '{{payment_status}}',
            'payment_sum' => '{{payment_sum}}',
            'utm_source' => '{{utm_source}}',
            'utm_medium' => '{{utm_medium}}',
            'utm_campaign' => '{{utm_campaign}}',
            'utm_content' => '{{utm_content}}',
            'utm_term' => '{{utm_term}}',
            'ip' => '{{ip}}',
            'fio' => '{{fio}}',
            'country' => '{{country}}',
            'postIndex' => '{{postIndex}}',
            'region' => '{{region}}',
            'city' => '{{city}}',
            'address' => '{{address}}',
            'phone' => '{{phone}}',
            'email' => '{{email}}',
            'quantity' => '{{quantity}}',
            'comment' => '{{comment}}',
            'status' => '{{status}}',
        ];
    }

    /**
     * @return string
     */
    public function getIdParam(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public function getStatusParam(): string
    {
        return 'status';
    }

    /**
     * @param string $status
     * @return int
     * @throws Exception
     */
    public function getMappedStatus(string $status): int
    {
        if (isset(self::STATUSES[$status])) {
            return self::STATUSES[$status];
        }

        throw new Exception('Неизвесный статус: ' . $status);
    }

    /**
     * @return string
     */
    public function getCountryParam(): string
    {
        return 'country';
    }
}
