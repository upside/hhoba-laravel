<?php

namespace App\AdvertisersApi\LeadVertex;

/**
 * Class LeadVertexPostBackVo
 * @package App\AdvertisersApi\LeadVertex
 */
class LeadVertexPostBackVo
{
    /**
     * ID заказа
     * @var
     */
    private $id;

    /**
     * дата и время соврешения заказа
     * @var
     */
    private $datetime;

    /**
     * домен, на котором был совершен заказ
     * @var
     */
    private $domain;

    /**
     * цена проданного товара
     * @var
     */
    private $price;

    /**
     * статус вознаграждения. -1 - отказано, 0 - в обработке, 1 - выплачено
     * @var int
     */
    private $paymentStatus;

    /**
     * сумма вознаграждения за заказ
     * @var
     */
    private $paymentSum;

    /**
     * метка utm_source
     * @var
     */
    private $utmSource;

    /**
     * метка utm_medium
     * @var
     */
    private $utmMedium;

    /**
     * метка utm_campaign
     * @var
     */
    private $utmCampaign;

    /**
     * метка utm_content
     * @var
     */
    private $utmContent;

    /**
     * метка utm_term
     * @var
     */
    private $utmTerm;

    /**
     * IP клиента
     * @var string
     */
    private $ip;
    private $fio;
    private $country;
    private $postIndex;
    private $region;
    private $city;
    private $address;
    private $phone;
    private $email;
    private $quantity;
    private $comment;

    /**
     * возвращает группу статуса заказа. Этот параметр может принимать значения:
     * spam - Ошибка/Спам/Дубль
     * return - Возврат
     * canceled - Отменен
     * processing - Обработка
     * accepted - Принят
     * shipped - Отправлен
     * paid - Оплачен
     * null - если функционал получения группы статуса заказа для вас недоступен
     *
     * @var string
     */
    private $status;

    public function __construct(
        ?string $id,
        ?string $datetime,
        ?string $domain,
        $price,
        int $paymentStatus,
        $paymentSum,
        ?string $utmSource,
        ?string $utmMedium,
        ?string $utmCampaign,
        ?string $utmContent,
        ?string $utmTerm,
        ?string $ip,
        ?string $fio,
        ?string $country,
        ?string $postIndex,
        ?string $region,
        ?string $city,
        ?string $address,
        ?string $phone,
        ?string $email,
        int $quantity,
        ?string $comment,
        string $status
    )
    {

        $this->id = $id;
        $this->datetime = $datetime;
        $this->domain = $domain;
        $this->price = $price;
        $this->paymentStatus = $paymentStatus;
        $this->paymentSum = $paymentSum;
        $this->utmSource = $utmSource;
        $this->utmMedium = $utmMedium;
        $this->utmCampaign = $utmCampaign;
        $this->utmContent = $utmContent;
        $this->utmTerm = $utmTerm;
        $this->ip = $ip;
        $this->fio = $fio;
        $this->country = $country;
        $this->postIndex = $postIndex;
        $this->region = $region;
        $this->city = $city;
        $this->address = $address;
        $this->phone = $phone;
        $this->email = $email;
        $this->quantity = $quantity;
        $this->comment = $comment;
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDatetime(): ?string
    {
        return $this->datetime;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getPaymentStatus(): int
    {
        return $this->paymentStatus;
    }

    /**
     * @return mixed
     */
    public function getPaymentSum()
    {
        return $this->paymentSum;
    }

    /**
     * @return string|null
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @return string|null
     */
    public function getUtmContent(): ?string
    {
        return $this->utmContent;
    }

    /**
     * @return string|null
     */
    public function getUtmTerm(): ?string
    {
        return $this->utmTerm;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @return string|null
     */
    public function getFio(): ?string
    {
        return $this->fio;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getPostIndex(): ?string
    {
        return $this->postIndex;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

}
