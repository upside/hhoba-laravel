<?php

namespace App\AdvertisersApi;

/**
 * Class AdvertiserApiFactory
 * @package App\AdvertisersApi
 */
class AdvertiserApiFactory
{

    /**
     * @param int $id
     * @return AdvertisersApiInterface
     * @throws \Exception
     */
    public static function getApi(int $id): AdvertisersApiInterface
    {
        $advertisers = config('advertisers.api');

        if (isset($advertisers[$id])) {
            return new $advertisers[$id];
        }

        throw new \Exception('Апи ненайденно');
    }

}
