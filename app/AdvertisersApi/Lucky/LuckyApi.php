<?php

namespace App\AdvertisersApi\Lucky;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Models\Advertiser;
use App\Models\Lead;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

/**
 * Class LuckyApi
 * @package App\AdvertisersApi\Lucky
 */
class LuckyApi implements AdvertisersApiInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_TRASH = 'trash';

    public const STATUSES = [
        self::STATUS_NEW => Lead::STATUS_NEW,
        self::STATUS_REJECTED => Lead::STATUS_DECLINE,
        self::STATUS_CONFIRMED => Lead::STATUS_CONFIRM,
        self::STATUS_TRASH => Lead::STATUS_TRASH,
    ];

    /**
     * @param Lead $lead
     * @throws GuzzleException
     */
    public function send(Lead $lead)
    {
        $client = new Client();
        $offer = $lead->click->stream->offer;
        $advertiser = $offer->advertiser;

        $formParams = [
            'name' => $lead->name,
            'phone' => $lead->phone,
            'ip' => $lead->ip,
            'userAgent' => $lead->user_agent,
            'campaignHash' => $offer->params['campaignHash'],
            'country_call' => $lead->geo->iso_2,
        ];

        $query = [
            'api_key' => $advertiser->params['api_key'],
            'subid1' => $lead->click->tracker_id ?? '',
        ];

        $response = $client->request('POST', $this->getApiUrl(), [
            'query' => $query,
            'form_params' => $formParams,
        ]);

        $data = json_decode($response->getBody(), true);

        if ($data['success']) {
            $lead->cpa_id = $data['data']['click_id'];
            $lead->save();
            Log::channel($this->getName())->info($response->getBody());
        } else {
            Log::channel($this->getName())->error($response->getBody());
            Log::channel('syslog-telegram')->error('Неудалось отпраить лид: ' . $lead->id . ' Ответ от Lucky ' . $response->getBody());
        }

    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'lucky-online';
    }

    /**
     * @return array
     */
    public function getOfferParams(): array
    {
        return [
            'campaignHash' => ''
        ];
    }

    /**
     * @return array
     */
    public function getAdvertiserParams(): array
    {
        return [
            'api_key' => '',
        ];
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'https://lucky.online/api-webmaster/lead.html';
    }

    /**
     * @param Advertiser $advertiser
     * @return string
     */
    public function getPostBackUrl(Advertiser $advertiser): string
    {
        return route('post-back.receive', array_merge(['advertiser' => $advertiser], $this->getPostBackParams()));
    }

    /**
     * @return array
     */
    public function getPostBackParams(): array
    {
        return [
            'click_id' => '{click_id}',
            'status' => '{status}',
            'amount' => '{amount}',
            'click_country' => '{click.country}',
            'get_utm_source' => '{get.utm_source}',
            'get_utm_content' => '{get.utm_content}',
            'get_utm_term' => '{get.utm_term}',
            'get_utm_medium' => '{get.utm_medium}',
            'get_utm_campaign' => '{get.utm_campaign}',
            'get_subid1' => '{get.subid1}',
            'get_subid2' => '{get.subid2}',
            'get_subid3' => '{get.subid3}',
            'click_ts' => '{click_ts}',
            'lead_ts' => '{lead_ts}',
            'offer_id' => '{offer_id}',
            'click_ip' => '{click.ip}',
            'currency' => '{currency}',
        ];
    }

    /**
     * @return string
     */
    public function getIdParam(): string
    {
        return 'click_id';
    }

    /**
     * @return string
     */
    public function getStatusParam(): string
    {
        return 'status';
    }

    /**
     * @param string $status
     * @return int
     * @throws Exception
     */
    public function getMappedStatus(string $status): int
    {
        if (isset(self::STATUSES[$status])) {
            return self::STATUSES[$status];
        }

        throw new Exception('Неизвесный статус: ' . $status);
    }

    /**
     * @return string
     */
    public function getCountryParam(): string
    {
        return 'click_country';
    }
}
