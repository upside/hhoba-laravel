<?php

namespace App\AdvertisersApi\Lucky;

/**
 * Class LuckyPostBackVo
 * @package App\AdvertisersApi\Lucky
 */
class LuckyPostBackVo
{
    /**
     * @var int
     */
    private $click_id;
    /**
     * @var int
     */
    private $status;
    /**
     * @var int
     */
    private $amount;
    /**
     * @var string
     */
    private $click_country;
    /**
     * @var string|null
     */
    private $get_utm_source;
    /**
     * @var string|null
     */
    private $get_utm_content;
    /**
     * @var string|null
     */
    private $get_utm_term;
    /**
     * @var string|null
     */
    private $get_utm_medium;
    /**
     * @var string|null
     */
    private $get_utm_campaign;
    /**
     * @var string|null
     */
    private $get_subid1;
    /**
     * @var string|null
     */
    private $get_subid2;
    /**
     * @var string|null
     */
    private $get_subid3;
    /**
     * @var string|null
     */
    private $click_ts;
    /**
     * @var string|null
     */
    private $lead_ts;
    /**
     * @var int
     */
    private $offer_id;
    /**
     * @var string
     */
    private $currency;

    public function __construct(
        int $click_id,
        int $status,
        int $amount,
        string $click_country,
        ?string $get_utm_source,
        ?string $get_utm_content,
        ?string $get_utm_term,
        ?string $get_utm_medium,
        ?string $get_utm_campaign,
        ?string $get_subid1,
        ?string $get_subid2,
        ?string $get_subid3,
        ?string $click_ts,
        ?string $lead_ts,
        int $offer_id,
        string $currency
    )
    {

        $this->click_id = $click_id;
        $this->status = $status;
        $this->amount = $amount;
        $this->click_country = $click_country;
        $this->get_utm_source = $get_utm_source;
        $this->get_utm_content = $get_utm_content;
        $this->get_utm_term = $get_utm_term;
        $this->get_utm_medium = $get_utm_medium;
        $this->get_utm_campaign = $get_utm_campaign;
        $this->get_subid1 = $get_subid1;
        $this->get_subid2 = $get_subid2;
        $this->get_subid3 = $get_subid3;
        $this->click_ts = $click_ts;
        $this->lead_ts = $lead_ts;
        $this->offer_id = $offer_id;
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getClickId(): int
    {
        return $this->click_id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getClickCountry(): string
    {
        return $this->click_country;
    }

    /**
     * @return string|null
     */
    public function getGetUtmSource(): ?string
    {
        return $this->get_utm_source;
    }

    /**
     * @return string|null
     */
    public function getGetUtmContent(): ?string
    {
        return $this->get_utm_content;
    }

    /**
     * @return string|null
     */
    public function getGetUtmTerm(): ?string
    {
        return $this->get_utm_term;
    }

    /**
     * @return string|null
     */
    public function getGetUtmMedium(): ?string
    {
        return $this->get_utm_medium;
    }

    /**
     * @return string|null
     */
    public function getGetUtmCampaign(): ?string
    {
        return $this->get_utm_campaign;
    }

    /**
     * @return string|null
     */
    public function getGetSubid1(): ?string
    {
        return $this->get_subid1;
    }

    /**
     * @return string|null
     */
    public function getGetSubid2(): ?string
    {
        return $this->get_subid2;
    }

    /**
     * @return string|null
     */
    public function getGetSubid3(): ?string
    {
        return $this->get_subid3;
    }

    /**
     * @return string|null
     */
    public function getClickTs(): ?string
    {
        return $this->click_ts;
    }

    /**
     * @return string|null
     */
    public function getLeadTs(): ?string
    {
        return $this->lead_ts;
    }

    /**
     * @return int
     */
    public function getOfferId(): int
    {
        return $this->offer_id;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }
}
