<?php

namespace App\AdvertisersApi;

use App\Models\Advertiser;
use App\Models\Lead;

/**
 * Interface AdvertisersApiInterface
 * @package App\AdvertisersApi
 */
interface AdvertisersApiInterface
{
    /**
     * @param Lead $lead
     * @return mixed
     */
    public function send(Lead $lead);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getApiUrl(): string;

    /**
     * @return array
     */
    public function getOfferParams(): array;

    /**
     * @return array
     */
    public function getAdvertiserParams(): array;

    /**
     * @return array
     */
    public function getPostBackParams(): array;

    /**
     * @param Advertiser $advertiser
     * @return string
     */
    public function getPostBackUrl(Advertiser $advertiser): string;

    /**
     * @return string
     */
    public function getIdParam(): string;

    /**
     * @return string
     */
    public function getStatusParam(): string;

    /**
     * @return string
     */
    public function getCountryParam(): string;

    /**
     * @param string $status
     * @return int
     */
    public function getMappedStatus(string $status): int;

}
