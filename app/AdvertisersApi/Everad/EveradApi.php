<?php

namespace App\AdvertisersApi\Everad;

use App\AdvertisersApi\AdvertisersApiInterface;
use App\Models\Advertiser;
use App\Models\Lead;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

/**
 * Class EveradApi
 * @package App\AdvertisersApi\Everad
 */
class EveradApi implements AdvertisersApiInterface
{
    public const STATUS_NEW = 'pending';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_TRASH = 'trash';

    public const STATUSES = [
        self::STATUS_NEW => Lead::STATUS_NEW,
        self::STATUS_APPROVED => Lead::STATUS_CONFIRM,
        self::STATUS_REJECTED => Lead::STATUS_DECLINE,
        self::STATUS_TRASH => Lead::STATUS_TRASH,
    ];

    /**
     * @param Lead $lead
     * @throws GuzzleException
     */
    public function send(Lead $lead)
    {
        $client = new Client();

        $offer = $lead->click->stream->offer;

        $formParams = [
            'campaign_id' => $offer->params['campaign_id'],
            'ip' => $lead->ip,
            'name' => $lead->name,
            'phone' => $lead->phone,
            'sid1' => $lead->id,
        ];

        $response = $client->request('POST', $this->getApiUrl(), [
            'form_params' => $formParams,
        ]);

        $data = json_decode($response->getBody(), true);

        Log::channel($this->getName())->info($response->getBody());

        if ($data['ok']) {
            $lead->cpa_id = $data['id'];
            $lead->save();
            Log::channel($this->getName())->info($response->getBody());
        } else {
            Log::channel($this->getName())->error($response->getBody());
            Log::channel('syslog-telegram')->error('Неудалось отпраить лид: ' . $lead->id . ' Ответ от Everad' . $response->getBody());
        }

    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Everad';
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return 'https://tracker.everad.com/conversion/new';
    }

    /**
     * @return array
     */
    public function getOfferParams(): array
    {
        return [
            'campaign_id' => ''
        ];
    }

    /**
     * @return array
     */
    public function getAdvertiserParams(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getPostBackParams(): array
    {
        return [
            'id' => '{id}',
            'date' => '{date}',
            'campaign_id' => '{campaign_id}',
            'offer_id' => '{offer_id}',
            'country_code' => '{country_code}',
            'ip' => '{ip}',
            'traffic_type' => '{traffic_type}',
            'sid1' => '{sid1}',
            'sid2' => '{sid2}',
            'sid3' => '{sid3}',
            'sid4' => '{sid4}',
            'sid5' => '{sid5}',
            'status' => '{status}',
            'payout' => '{payout}',
            'currency' => '{currency}',
        ];
    }

    /**
     * @param Advertiser $advertiser
     * @return string
     */
    public function getPostBackUrl(Advertiser $advertiser): string
    {
        return route('post-back.receive', array_merge(['advertiser' => $advertiser], $this->getPostBackParams()));
    }

    /**
     * @return string
     */
    public function getIdParam(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public function getStatusParam(): string
    {
        return 'status';
    }

    /**
     * @param string $status
     * @return int
     * @throws Exception
     */
    public function getMappedStatus(string $status): int
    {
        if (isset(self::STATUSES[$status])) {
            return self::STATUSES[$status];
        }

        throw new Exception('Неизвесный статус: ' . $status);
    }

    /**
     * @return string
     */
    public function getCountryParam(): string
    {
        return 'country_code';
    }
}
