<?php

namespace App\Exceptions;

use App\Jobs\ProcessSendTelegramError;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            //app('sentry')->captureException($exception);

            $request = Request::instance();
            $user = Auth::user();

            //'date' => date('Y-m-d H:s'),
            //'host' => $request->getHost(),
            //'ip' => $request->ip(),
            //'url' => $request->fullUrl(),
            //'post' => $request->post(),
            //'get' => $this->request->get(),
            //'user_id' => $user->id ?? '-',
            //'user_email' => $user->email ?? '-',
            //'code' => $exception->getCode(),
            //'file' => $exception->getFile(),
            //'line' => $exception->getLine(),
            //'message' => $exception->getMessage(),

            $message = '';
            //$message .= 'Дата: ' . date('Y-m-d H:s') . PHP_EOL;
            $message .= 'host: ' . $request->getHost() . PHP_EOL;
            $message .= 'ip: ' . $request->ip() . PHP_EOL;
            $message .= 'url: ' . $request->fullUrl() . PHP_EOL;
            $message .= 'user_id: ' . ($user->id ?? '-') . PHP_EOL;
            $message .= 'user_email: ' . ($user->email ?? '-') . PHP_EOL;
            $message .= 'code: ' . $exception->getCode() . PHP_EOL;
            $message .= 'file: ' . $exception->getFile() . PHP_EOL;
            $message .= 'line: ' . $exception->getLine() . PHP_EOL;
            $message .= 'message: ' . $exception->getMessage() . PHP_EOL;

            ProcessSendTelegramError::dispatch('-391445879', $message);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
