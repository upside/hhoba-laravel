<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('permission', function (User $user, string $slug) {

            foreach ($user->roles as $role) {
                foreach ($role->permissions as $permission) {
                    if ($permission->slug === $slug) {
                        return true;
                    }
                }
            }

            return false;
        });

        Gate::define('role', function (User $user, string $slug) {

            foreach ($user->roles as $role) {
                if ($role->slug === $slug) {
                    return true;
                }
            }

            return false;
        });

    }
}
