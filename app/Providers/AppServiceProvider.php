<?php

namespace App\Providers;

use App\Models\Lead;
use App\Models\UserBalance;
use App\Observers\LeadObserver;
use App\Observers\UserBalanceObserver;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Lead::observe(LeadObserver::class);
        UserBalance::observe(UserBalanceObserver::class);
        Resource::withoutWrapping();
    }
}
