require('./bootstrap');
//require('admin-lte');
import Vue from 'vue';

import store from './store';
import router from './router';
import App from "./components/App";


const app = new Vue({
    el: '#app',
    store,
    router,
    components: {
        App,
    },
    beforeMount() {
        Echo.private('user.' + document.hhoba.user.id).listen('AppEvent', (e) => {
            this.$store.commit(e.type, e.data);
        });
        this.$store.commit('setUser', document.hhoba.user);
    }
});

