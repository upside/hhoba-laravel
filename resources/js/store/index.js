import Vue from 'vue';
import Vuex from 'vuex';
import {LEAD_RECEIVE, LEAD_UPDATE_STATUS, USER_BALANCE_UPDATE} from '../events';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        app: {
            user: null,
            pageTitle: 'Консоль',
            sideBar: {
                open: true
            },
            breadcrumbs:[],
        },
        events: {
            [USER_BALANCE_UPDATE]: {},
            [LEAD_RECEIVE]: {},
            [LEAD_UPDATE_STATUS]: {},
        },
        geo: {
            loaded: false,
            data:[],
        },
    },
    mutations: mutations,
    actions: actions,
    getters: {
        getUserBalances: state => {
            return state.events[USER_BALANCE_UPDATE];
        },
        getUser: state => {
            return state.app.user;
        },
        checkRole: state => slug => {
            let check = false;
            if (state.app.user.roles !== null) {
                state.app.user.roles.forEach(role => {
                    if (role.slug === slug) {
                        check = true;
                    }
                });
            }
            return check;
        },
        getGeoData: state => {
            return state.geo;
        },
        getApp: state => {
            return state.app;
        },
    }
});

export default store;
