import geoActions from "./geo";

const actions = {
    ...geoActions,
    toggleSidebar({commit, state}) {
        state.app.sideBar.open ? document.body.classList.add('sidebar-collapse') : document.body.classList.remove('sidebar-collapse');
        commit('toggleSidebar');
    },
    setPageTitle({commit}, title)
    {
        commit('setPageTitle', title);
    },
    setBreadcrumbs({commit}, breadcrumbs)
    {
        commit('setBreadcrumbs', breadcrumbs);
    }
};

export default actions;
