const geoActions = {
    geoList({commit, state}, page) {
        commit('toggleLoaded', false);
        axios.get(`/admin/api/geo/list?page=${page}`)
            .then((result) => {
                return result.data;
            })
            .then((rowData) => {
                commit('geoList', rowData);
                commit('toggleLoaded', true);
            });
    }

};

export default geoActions;
