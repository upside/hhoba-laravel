
const geoMutations = {
    geoList(state, payload) {
        state.geo.data = payload.data;
        state.geo.links = payload.links;
        state.geo.meta = payload.meta;
    },
    toggleLoaded(state, payload){
        state.geo.loaded = payload;
    }
};

export  default geoMutations;
