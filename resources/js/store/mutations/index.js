import {LEAD_RECEIVE, LEAD_UPDATE_STATUS, USER_BALANCE_UPDATE} from '../../events';
import geoMutations from "./geo";

const mutations = {
    ...geoMutations,
    [USER_BALANCE_UPDATE](state, payload) {
        state.events[USER_BALANCE_UPDATE][payload.currency.code] = payload.amount;
    },
    [LEAD_RECEIVE](state, payload) {
        state.events[LEAD_RECEIVE] = payload;
    },
    [LEAD_UPDATE_STATUS](state, payload) {
        state.events[LEAD_UPDATE_STATUS] = payload;
    },
    toggleSidebar(state) {
        state.app.sideBar.open = !state.app.sideBar.open;
    },
    setUser(state, payload) {
        state.app.user = payload;

        payload.balances.forEach((item => {
            state.events[USER_BALANCE_UPDATE][item.currency.code] = item.amount;
        }));
    },
    setPageTitle(state, payload) {
        state.app.pageTitle = payload;
    },
    setBreadcrumbs(state, payload) {
        state.app.breadcrumbs = payload;
    }
};


export default mutations;
