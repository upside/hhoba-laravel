import DashboardPage from '../components/pages/DashboardPage';

import PermissionPage from '../components/pages/PermissionPage';
import RolePage from '../components/pages/RolePage';
import StatisticsPage from '../components/pages/StatisticsPage';

import CurrencyPage from '../components/currency/Page';
import CurrencyList from '../components/currency/List';
import CurrencyEdit from '../components/currency/Edit';
import CurrencyAdd from '../components/currency/Add';

import ClickPage from '../components/click/Page';
import ClickList from '../components/click/List';
import ClickEdit from '../components/click/Edit';
import ClickAdd from '../components/click/Add';

import UserPage from '../components/user/Page';
import UserList from '../components/user/List';
import UserEdit from '../components/user/Edit';
import UserAdd from '../components/user/Add';
import UserShow from '../components/user/Show';

import AdvertiserPage from '../components/advertiser/Page';
import AdvertiserList from '../components/advertiser/List';
import AdvertiserEdit from '../components/advertiser/Edit';
import AdvertiserAdd from '../components/advertiser/Add';

import GeoPage from '../components/geo/Page';
import GeoList from '../components/geo/List';
import GeoEdit from '../components/geo/Edit';
import GeoAdd from '../components/geo/Add';

import StreamPage from '../components/stream/Page';
import StreamList from '../components/stream/List';
import StreamEdit from '../components/stream/Edit';
import StreamAdd from '../components/stream/Add';

import FinancePage from '../components/finance/Page';
import FinanceList from '../components/finance/List';

import OfferPage from '../components/offer/Page';
import OfferList from '../components/offer/List';
import OfferEdit from '../components/offer/Edit';
import OfferAdd from '../components/offer/Add';
import OfferShow from '../components/offer/Show';

import LeadPage from '../components/lead/Page';
import LeadList from '../components/lead/List';

const routes = [
    {
        path: '/advertiser',
        component: AdvertiserPage,
        children: [
            {
                name: 'advertiser',
                path: '',
                component: AdvertiserList,
            },
            {
                name: 'advertiser_page',
                path: 'page/:num',
                component: AdvertiserList,
            },
            {
                name: 'advertiser_edit',
                path: 'edit/:id',
                component: AdvertiserEdit,
            },
            {
                name: 'advertiser_add',
                path: 'add',
                component: AdvertiserAdd,
            },
        ],
    },
    {
        path: '/currency',
        component: CurrencyPage,
        children: [
            {
                name: 'currency',
                path: '',
                component: CurrencyList,
            },
            {
                name: 'currency_page',
                path: 'page/:num',
                component: CurrencyList,
            },
            {
                name: 'currency_edit',
                path: 'edit/:id',
                component: CurrencyEdit,
            },
            {
                name: 'currency_add',
                path: 'add',
                component: CurrencyAdd,
            },
        ],
    },
    {
        path: '/finance',
        component: FinancePage,
        children: [
            {
                name: 'finance',
                path: '',
                component: FinanceList,
            },
            {
                name: 'finance_page',
                path: 'page/:num',
                component: FinanceList,
            },
        ],
    },
    {
        path: '/geo',
        component: GeoPage,
        children: [
            {
                name: 'geo',
                path: '',
                component: GeoList,
            },
            {
                name: 'geo_page',
                path: 'page/:num',
                component: GeoList,
            },
            {
                name: 'geo_edit',
                path: 'edit/:id',
                component: GeoEdit,
            },
            {
                name: 'geo_add',
                path: 'add',
                component: GeoAdd,
            },
        ],
    },
    {
        path: '/lead',
        component: LeadPage,
        children: [
            {
                name: 'lead',
                path: '',
                component: LeadList,
            },
            {
                name: 'lead_page',
                path: 'page/:num',
                component: LeadList,
            },
        ],
    },
    {
        path: '/offer',
        component: OfferPage,
        children: [
            {
                name: 'offer',
                path: '',
                component: OfferList,
            },
            {
                name: 'offer_page',
                path: 'page/:num',
                component: OfferPage,
            },
            {
                name: 'offer_edit',
                path: 'edit/:id',
                component: OfferEdit,
            },
            {
                name: 'offer_add',
                path: 'add',
                component: OfferAdd,
            },
            {
                name: 'offer_show',
                path: ':id',
                component: OfferShow,
            },
        ],
    },
    {
        path: '/permission',
        component: PermissionPage,
        name: 'permission'
    },
    {
        path: '/role',
        component: RolePage,
        name: 'role'
    },
    {
        path: '/statistics',
        component: StatisticsPage,
        name: 'statistics'
    },
    {
        path: '/stream',
        component: StreamPage,
        children: [
            {
                name: 'stream',
                path: '',
                component: StreamList,
            },
            {
                name: 'stream_page',
                path: 'page/:num',
                component: StreamList,
            },
            {
                name: 'stream_edit',
                path: 'edit/:id',
                component: StreamEdit,
            },
            {
                name: 'stream_add',
                path: 'add',
                component: StreamAdd,
            },
        ],
    },
    {
        path: '/user',
        component: UserPage,
        children: [
            {
                name: 'user',
                path: '',
                component: UserList,
            },
            {
                name: 'user_page',
                path: 'page/:num',
                component: UserShow,
            },
            {
                name: 'user_edit',
                path: 'edit/:id',
                component: UserEdit,
            },
            {
                name: 'user_add',
                path: 'add',
                component: UserAdd,
            },
            {
                name: 'user_show',
                path: ':id',
                component: UserShow,
            },
        ],
    },
    {
        path: '/click',
        component: ClickPage,
        children: [
            {
                name: 'click',
                path: '',
                component: ClickList,
            },
            {
                name: 'click_page',
                path: 'page/:num',
                component: ClickList,
            },
            {
                name: 'click_edit',
                path: 'edit/:id',
                component: ClickEdit,
            },
            {
                name: 'click_add',
                path: 'add',
                component: ClickAdd,
            },
        ],
    },
    {
        path: '/',
        component: DashboardPage,
        name: 'dashboard'
    },
];

export default routes;
