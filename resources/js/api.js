export const apiStore = function (resource, data) {
    return axios.post(`/admin/api/${resource}`, data);
};

export const apiIndex = function (resource, page) {
    return axios.get(`/admin/api/${resource}?page=${page}`);
};

export const apiAll = function (resource) {
    return axios.get(`/admin/api/${resource}/all`);
};

export const apiShow = function (resource, id) {
    return axios.get(`/admin/api/${resource}/${id}`);
};

export const apiDestroy = function (resource, id) {
    return axios.delete(`/admin/api/${resource}/${id}`);
};

export const apiUpdate = function (resource, id, data) {
    return axios.put(`/admin/api/${resource}/${id}`, data);
};

