<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('App\Http\Controllers\Admin\Api')->prefix('admin/api')->as('admin.')->middleware(['auth'])->group(function () {

    Route::get('console/lead-stat', 'ConsoleController@leadStat')->name('console.lead-stat');

    Route::apiResource('advertiser', 'AdvertiserController');
    Route::apiResource('statistics', 'StatisticsController')->only(['index']);
    Route::apiResource('balance-log', 'BalanceLogController')->only(['index']);
    Route::apiResource('click', 'ClickController');

    Route::get('currency/all', 'CurrencyController@all')->name('currency.all');
    Route::apiResource('currency', 'CurrencyController');

    Route::get('geo/all', 'GeoController@all')->name('geo.all');
    Route::apiResource('geo', 'GeoController');

    Route::apiResource('lead', 'LeadController');

    Route::get('offer/lead-stat/{offer}', 'OfferController@leadStat')->name('offer.lead-stat');
    Route::apiResource('offer', 'OfferController');

    Route::apiResource('permission', 'PermissionController');
    Route::apiResource('role', 'RoleController');
    Route::apiResource('stream', 'StreamController');

    Route::get('user/all', 'UserController@all')->name('user.all');
    Route::apiResource('user', 'UserController');
});

Route::get('login', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', '\App\Http\Controllers\Auth\LoginController@login');
Route::any('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('admin{any}', '\App\Http\Controllers\Admin\SpaController@index')->middleware(['auth'])->where('any', '.*');


