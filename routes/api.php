<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['cors'])->group(function () {
    Route::any('click/{stream}', '\App\Http\Controllers\Api\Click@deliver')->name('click.deliver');
    Route::any('lead', '\App\Http\Controllers\Api\Lead@receive')->name('lead.receive');
    Route::any('post-back/{advertiser}', '\App\Http\Controllers\Api\PostBack@receive')->name('post-back.receive');
    Route::any('cloak/{apiKey}/{siteId}', '\App\Http\Controllers\Api\Cloak@index')->name('cloak.index');
});
